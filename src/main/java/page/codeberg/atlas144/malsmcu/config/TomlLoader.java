// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config;

import com.moandjiezana.toml.Toml;
import java.io.InputStream;

/**
 *
 * @author atlas144
 */
public abstract class TomlLoader {
    private final Toml toml;
    
    protected TomlLoader(String path) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classloader.getResourceAsStream(path);
        toml = new Toml().read(stream);
    }
    
    public Toml getToml() {
        return toml;
    }
}
