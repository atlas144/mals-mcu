// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.hardware;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class Halls extends TomlLoader {
    private static final String FILE = "commons/hardware/halls.toml";
    
    private static Halls loader;
    
    private Halls() {
        super(FILE);
    }
    
    public static Halls getInsance() {
        if (loader == null) {
            loader = new Halls();
        }
        
        return loader;
    }
}
