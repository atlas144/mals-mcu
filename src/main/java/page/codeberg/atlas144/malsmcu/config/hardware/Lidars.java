// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.hardware;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class Lidars extends TomlLoader {
    private static final String FILE = "commons/hardware/lidars.toml";
    
    private static Lidars loader;
    
    private Lidars() {
        super(FILE);
    }
    
    public static Lidars getInsance() {
        if (loader == null) {
            loader = new Lidars();
        }
        
        return loader;
    }
}
