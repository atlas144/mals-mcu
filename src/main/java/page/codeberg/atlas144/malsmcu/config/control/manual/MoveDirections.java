// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.control.manual;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class MoveDirections extends TomlLoader {
    private static final String FILE = "commons/control/manual/move_directions.toml";
    
    private static MoveDirections loader;
    
    private MoveDirections() {
        super(FILE);
    }
    
    public static MoveDirections getInsance() {
        if (loader == null) {
            loader = new MoveDirections();
        }
        
        return loader;
    }
}
