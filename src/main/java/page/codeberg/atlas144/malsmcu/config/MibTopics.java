// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config;

/**
 *
 * @author atlas144
 */
public class MibTopics extends TomlLoader {
    private static final String FILE = "commons/topics.toml";
    
    private static MibTopics loader;
    
    private MibTopics() {
        super(FILE);
    }
    
    public static MibTopics getInsance() {
        if (loader == null) {
            loader = new MibTopics();
        }
        
        return loader;
    }
}
