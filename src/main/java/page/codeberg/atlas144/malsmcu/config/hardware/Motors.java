// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.hardware;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class Motors extends TomlLoader {
    private static final String FILE = "commons/hardware/motors.toml";
    
    private static Motors loader;
    
    private Motors() {
        super(FILE);
    }
    
    public static Motors getInsance() {
        if (loader == null) {
            loader = new Motors();
        }
        
        return loader;
    }
}
