// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.hardware;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class Tactiles extends TomlLoader {
    private static final String FILE = "commons/hardware/tactiles.toml";
    
    private static Tactiles loader;
    
    private Tactiles() {
        super(FILE);
    }
    
    public static Tactiles getInsance() {
        if (loader == null) {
            loader = new Tactiles();
        }
        
        return loader;
    }
}
