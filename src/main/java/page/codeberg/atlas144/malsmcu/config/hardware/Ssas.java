// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config.hardware;

import page.codeberg.atlas144.malsmcu.config.*;

/**
 *
 * @author atlas144
 */
public class Ssas extends TomlLoader {
    private static final String FILE = "commons/hardware/ssas.toml";
    
    private static Ssas loader;
    
    private Ssas() {
        super(FILE);
    }
    
    public static Ssas getInsance() {
        if (loader == null) {
            loader = new Ssas();
        }
        
        return loader;
    }
}
