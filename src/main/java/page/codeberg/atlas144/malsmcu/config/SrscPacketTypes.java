// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.config;

/**
 *
 * @author atlas144
 */
public class SrscPacketTypes extends TomlLoader {
    private static final String FILE = "srsc_packet_types.toml";
    
    private static SrscPacketTypes loader;
    
    private SrscPacketTypes() {
        super(FILE);
    }
    
    public static SrscPacketTypes getInsance() {
        if (loader == null) {
            loader = new SrscPacketTypes();
        }
        
        return loader;
    }
}
