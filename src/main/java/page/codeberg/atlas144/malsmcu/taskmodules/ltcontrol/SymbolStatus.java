// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol;

/**
 *
 * @author atlas144
 */
public class SymbolStatus {
    private static final int PASS_THRESHOLD = 5;
    
    private boolean symbolInProcess;
    private int imagesWithoutSymbol;
    
    public SymbolStatus() {
        symbolInProcess = false;
        imagesWithoutSymbol = PASS_THRESHOLD;
    }
    
    public boolean postSymbolDelayPassed() {
        return imagesWithoutSymbol >= PASS_THRESHOLD;
    }
    
    public void imageWithSymbol() {
        imagesWithoutSymbol = 0;
    }
    
    public void imageWithoutSymbol() {
        imagesWithoutSymbol++;
    }
    
    public boolean isSymbolInProcess() {
        return symbolInProcess;
    }
    
    public void symbolProcessing() {
        symbolInProcess = true;
        imagesWithoutSymbol = 0;
    }
    
    public void symbolProcessingFinished() {
        symbolInProcess = false;
    }
}
