// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.websocket;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.taskmodules.websocket.model.Action;
import static page.codeberg.atlas144.malsmcu.taskmodules.websocket.model.Action.SUBSCRIBE;

/**
 *
 * @author atlas144
 */
public class WebSocketModule extends TaskModule {
    private static final String HOST = "localhost";
    private static final short PORT = 14400;
    
    private Server server;
    
    private final OnMessageListener onMessageListener = (WebSocket Connection, String messageJson) -> {
        try {
            JSONObject messageObject = new JSONObject(messageJson);
            Action action = messageObject.getEnum(Action.class, "action");
            String topic = messageObject.getString("topic");

            Logger.debug("{}: accepted message - action: {}, topic: {}", getModuleName(), action, topic);
            
            switch (action) {
                case PUBLISH -> {
                    Priority priority = messageObject.getEnum(Priority.class, "priority");
                    
                    if (messageObject.has("payload")) {
                        broker.publish(topic, messageObject.getJSONObject("payload"), priority);
                    } else {
                        broker.publish(topic, priority);
                    }
                }
                case SUBSCRIBE -> broker.subscribe(topic, this);
                default -> Logger.info("{}: arrived message with unknown action: {}", getModuleName(), action);
            }
        } catch (JSONException e) {
            Logger.info("{}: JSON parsing error: {} (message: {})", getModuleName(), e.getMessage(), messageJson);
        }
    };

    public WebSocketModule() {
        super("WebSocketModule");
    }

    @Override
    protected void setup() {
        server = new Server(HOST, PORT, broker);
        server.registerOnMessageListener(onMessageListener);
        server.start();
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        
        if (server.isConnected()) {
            JSONObject messageObject = new JSONObject();
            
            messageObject.put("topic", message.topic());
            messageObject.put("payload", message.payload());
            messageObject.put("priority", message.priority());
                        
            server.sendMessage(messageObject.toString());
            Logger.debug("{}: message successfully sent to topic '{}'", getModuleName(), message.topic());
        }
    }
}
