// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.api;

import com.moandjiezana.toml.Toml;
import io.javalin.http.Context;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class LtService {
    private final MalsIntermoduleBroker broker;
    private final Toml topics;
    
    public LtService(MalsIntermoduleBroker broker) {
        this.broker = broker;
        topics = MibTopics.getInsance().getToml();
    }
    
    public void symbolProcessed(Context context) {
        broker.publish(topics.getString("lt.symbolProcessingFinished"), Priority.NORMAL);
        Logger.info("LT symbol processing finished");
        context.status(200);
    }
}
