// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.io.gpio.digital.PullResistance;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Tactile {
    private static final String PROVIDER = "pigpio-digital-input";
    
    private final String id;
    private final MalsIntermoduleBroker broker;
    private final DigitalInput hall;
    private final String topic;

    public Tactile(Context context, String id, MalsIntermoduleBroker broker, int pin, String topic) {
        this.id = id;
        this.broker = broker;
        this.topic = topic;
        hall = context.create(DigitalInput.newConfigBuilder(context)
            .address(pin)
            .provider(PROVIDER)
            .pull(PullResistance.PULL_DOWN)
            .build()
        );
        
        hall.addListener(e -> {
            if (e.state() == DigitalState.HIGH) {
                precessChange();
            }
        });
    }
    
    private void precessChange() {
        Logger.debug("{} tactile sensor activated", id);
        broker.publish(topic, Priority.CRITICAL);
    }
}
