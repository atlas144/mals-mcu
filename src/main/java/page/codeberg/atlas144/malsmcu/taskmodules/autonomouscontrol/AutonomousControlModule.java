// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import com.moandjiezana.toml.Toml;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;
import page.codeberg.atlas144.malsmcu.model.MotorDirection;
import page.codeberg.atlas144.malsmcu.model.VirtualHall;
import page.codeberg.atlas144.malsmcu.model.VirtualMotor;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.MotorControlResult;

/**
 *
 * @author atlas144
 */
public class AutonomousControlModule extends TaskModule {
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml autonomousTopics = topics.getTable("autonomous");
    private final Toml hallsToml = topics.getTable("scu.nonTwiSensors.halls");
    private final Toml motorsToml = topics.getTable("scu.motors");
    
    private final String loadTopic = topics.getString("client.autonomousLoad");
    private final String unloadTopic = topics.getString("client.autonomousUnload");
    private final String clientDisconnectedTopic = topics.getString("client.disconnected");
    private final String startTopic = autonomousTopics.getString("start");
    private final String stopTopic = autonomousTopics.getString("stop");
    private final String setPathTopic = autonomousTopics.getString("setPath");
    
    private final List<VirtualMotor> motors;
    private final List<VirtualHall> halls;
    
    private final ControlService controlService;
    
    private boolean loaded;
    
    public AutonomousControlModule() {
        super("AutonomousControlModule");
        motors = motorsToml.toMap().values().stream().map(motorToml -> new VirtualMotor((Toml) motorToml)).toList();
        halls = hallsToml.toMap().values().stream().map(hallToml -> new VirtualHall((Toml) hallToml)).toList();
        controlService = new ControlService(this::setSpeeds);
    }
    
    @Override
    protected void setup() throws InterruptedException {
        broker.subscribe(loadTopic, this);
        broker.subscribe(unloadTopic, this);
        broker.subscribe(halls.stream().map(hall -> hall.getMibTopic()).toList(), this);
        broker.subscribe(autonomousTopics.toMap().values().stream().map(topic -> (String) topic).toList(), this);
        broker.subscribe(clientDisconnectedTopic, this);
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        
        if (loaded || message.topic().equals(loadTopic)) {
            if (controlService.isRunning()) {
                halls
                        .stream()
                        .filter(hall -> message.topic().equals(hall.getMibTopic()))
                        .findAny()
                        .ifPresent(hall -> controlService.increment(hall.getPosition()));
            }
            
            if (message.topic().equals(loadTopic)) {
                loaded = true;
            } else if (message.topic().equals(unloadTopic)) {
                controlService.stopControl();
                loaded = false;
            } else if (message.topic().equals(startTopic)) {
                controlService.startControl();
            } else if (message.topic().equals(stopTopic)) {
                controlService.stopControl();
            } else if (message.topic().equals(setPathTopic)) {
                controlService.setPath(message.payload());
            } else if (message.topic().equals(clientDisconnectedTopic)) {
                controlService.stopControl();
                loaded = false;
            }
        }
    }
    
    public void setSpeeds(MotorControlResult result) {
        motors.forEach(motor -> {
            int speed = 0;
            MotorDirection direction = MotorDirection.FORWARD;
                        
            switch (motor.getPosition().getHorizontalPosition()) {
                case LEFT -> {
                    speed = result.getLeftSpeed();
                    direction = result.getLeftDirection();
                }
                case RIGHT -> {
                    speed = result.getRightSpeed();
                    direction = result.getRightDirection();
                }
            }
            
            broker.publish(motor.getMibTopic(), new JSONObject(Map.of(
                    "speed", speed,
                    "direction", direction
            )), Priority.NORMAL);
        });
    }
}
