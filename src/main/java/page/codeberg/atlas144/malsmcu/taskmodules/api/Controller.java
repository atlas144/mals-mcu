// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.api;

import io.javalin.Javalin;

/**
 *
 * @author atlas144
 */
public class Controller {
    public Controller(Javalin api, ControlService controlService, LtService ltService) {
        api.get("/control/manual", context -> {
            controlService.manualLoad(context);
        });
        api.get("/control/lt", context -> {
            controlService.ltLoad(context);
        });
        api.get("/control/lt/symbolPrecessed", context -> {
            ltService.symbolProcessed(context);
        });
    }
}
