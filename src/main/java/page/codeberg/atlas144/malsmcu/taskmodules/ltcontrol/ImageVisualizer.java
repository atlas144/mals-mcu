// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol;

import java.util.Base64;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author atlas144
 */
public class ImageVisualizer {
    private static final Scalar PATH_COLOR = new Scalar(200, 0, 200);
    private static final int THICKNESS = 2;
    private static final int LINE_TYPE = Imgproc.LINE_8;
    private final Base64.Encoder encoder;
    
    private Mat image;
    
    public ImageVisualizer() {
        image = new Mat();
        encoder = Base64.getEncoder();
    }
    
    public void setImage(Mat image) {
        this.image = image;
    }
    
    public void drawPath(MatOfPoint path) {
        Imgproc.drawContours(image, List.of(path), 0, PATH_COLOR, THICKNESS, LINE_TYPE);
    }
    
    public void drawSymbol(Scalar color, Mat position) {
        double[] c = position.get(0, 0);
        Point center = new Point(Math.round(c[0]), Math.round(c[1]));
        int radius = (int) Math.round(c[2]);
        
        Imgproc.circle(image, center, radius, color, THICKNESS, LINE_TYPE);
    }
    
    public String getImageBase64() {
        MatOfByte byteImage = new MatOfByte();
        Imgcodecs.imencode(".jpg", image, byteImage);
        
        return encoder.encodeToString(byteImage.toArray());
    }
}
