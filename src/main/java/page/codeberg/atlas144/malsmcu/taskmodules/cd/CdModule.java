// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd;

import com.moandjiezana.toml.Toml;
import java.util.ArrayList;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.lidar.LidarCd;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.sonar.SonarCd;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.sonar.SonarDirection;

/**
 *
 * @author atlas144
 */
public class CdModule extends TaskModule {
    private static final int LIDAR_DISTANCE = 30;
    private static final int FACE_SONAR_DISTANCE = 30;
    private static final int BOTTOM_SONAR_DISTANCE = 15;
    
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml twiSensorsTopics = topics.getTable("scu.twiSensors");
    private final String tactileFTopic = topics.getString("scu.nonTwiSensors.tactileFront");
    private final String tactileBTopic = topics.getString("scu.nonTwiSensors.tactileBack");
    
    private final CollisionHandler handler;
    private final LidarCd lidarCd;
    private final SonarCd sonarCd1F;
    private final SonarCd sonarCd1B;
    private final SonarCd sonarCd2F;
    private final SonarCd sonarCd2B;
    private final SonarCd sonarCd3F;
    private final SonarCd sonarCd3B;
    private final SonarCd sonarCd4F;
    private final SonarCd sonarCd4B;

    public CdModule() {
        super("CdModule");
        
        handler = new CollisionHandler(broker);
        lidarCd = new LidarCd(getModuleName(), broker, handler, LIDAR_DISTANCE);
        sonarCd1F = new SonarCd(getModuleName(), handler, CdDirection.FRONT, SonarDirection.FACE, FACE_SONAR_DISTANCE);
        sonarCd1B = new SonarCd(getModuleName(), handler, CdDirection.FRONT, SonarDirection.FACE, FACE_SONAR_DISTANCE);
        sonarCd2F = new SonarCd(getModuleName(), handler, CdDirection.BACK, SonarDirection.FACE, FACE_SONAR_DISTANCE);
        sonarCd2B = new SonarCd(getModuleName(), handler, CdDirection.BACK, SonarDirection.FACE, FACE_SONAR_DISTANCE);
        sonarCd3F = new SonarCd(getModuleName(), handler, CdDirection.FRONT, SonarDirection.BOTTOM, BOTTOM_SONAR_DISTANCE);
        sonarCd3B = new SonarCd(getModuleName(), handler, CdDirection.FRONT, SonarDirection.BOTTOM, BOTTOM_SONAR_DISTANCE);
        sonarCd4F = new SonarCd(getModuleName(), handler, CdDirection.BACK, SonarDirection.BOTTOM, BOTTOM_SONAR_DISTANCE);
        sonarCd4B = new SonarCd(getModuleName(), handler, CdDirection.BACK, SonarDirection.BOTTOM, BOTTOM_SONAR_DISTANCE);
    }

    @Override
    protected void setup() throws InterruptedException {
        broker.subscribe(new ArrayList(twiSensorsTopics.toMap().values()), this);
        broker.subscribe(tactileFTopic, this);
        broker.subscribe(tactileBTopic, this);
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        
        if (message.topic().equals(twiSensorsTopics.getString("lidar"))) {
            lidarCd.processNewValue(message.payload().getDouble("distance"), message.payload().getDouble("angle"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaFlf"))) {
            sonarCd1F.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaFlb"))) {
            sonarCd1B.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaFrf"))) {
            sonarCd2F.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaFrb"))) {
            sonarCd2B.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaBlf"))) {
            sonarCd3F.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaBlb"))) {
            sonarCd3B.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaBrf"))) {
            sonarCd4F.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(twiSensorsTopics.getString("ssaBrb"))) {
            sonarCd4B.addDistance(message.payload().getInt("value"));
        } else if (message.topic().equals(tactileFTopic)) {
            broker.publish("/command/cd/collision", "0", Priority.CRITICAL);
        } else if (message.topic().equals(tactileBTopic)) {
            broker.publish("/command/cd/collision", "1", Priority.CRITICAL);
        }
    }
}
