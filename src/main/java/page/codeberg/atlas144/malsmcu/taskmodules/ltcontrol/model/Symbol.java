// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model;

import org.opencv.core.Scalar;

/**
 *
 * @author atlas144
 */
public enum Symbol {
    RED(new Scalar(0, 0, 255)),
    GREEN(new Scalar(0, 255, 0)),
    BLUE(new Scalar(255, 0, 0)),
    NONE(new Scalar(0, 0, 0));

    public final Scalar color;

    private Symbol(Scalar color) {
        this.color = color;
    }
}
