// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import java.util.Map;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Lidar extends TwiDevice {
    private static final int DATA_SIZE = 3;
    private static final int MEASUREMENT_PERIOD = 20;
    
    private final String topic;
    
    public Lidar(Context context, MalsIntermoduleBroker broker, int address, String topic) {
        super(context, broker, address, DATA_SIZE, MEASUREMENT_PERIOD);
        
        this.topic = topic;
    }
    
    @Override
    protected void processData(byte[] data) {
        short angle = data[0];
        int distance = data[1] + 256 * data[2];
        JSONObject jsonPayload = new JSONObject(Map.of(
                "angle", angle,
                "distance", distance
        ));
        
        broker.publish(topic, jsonPayload, Priority.NORMAL);
    }
}
