// SPDX-License-Identifier: GPL-3.0-only

package page.codeberg.atlas144.malsmcu.taskmodules.manualcontrol;

import com.moandjiezana.toml.Toml;
import java.util.Map;
import org.json.JSONObject;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;
import page.codeberg.atlas144.malsmcu.config.control.manual.MoveDirections;
/**
 *
 * @author atlas144
 */
 public class ManualControlModule extends TaskModule {
    private final Toml moveDirections = MoveDirections.getInsance().getToml();
     
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml motorTopics = topics.getTable("scu.motors");
    private final String moveTopic = topics.getString("manual.move");
    private final String loadTopic = topics.getString("client.manualLoad");
    private final String unloadTopic = topics.getString("client.manualUnload");
    private final String clientDisconnectedTopic = topics.getString("client.disconnected");
    private boolean loaded;
    
    private void makeMove(String moveDirection, int speed) {
        moveDirections
                .toMap()
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().equalsIgnoreCase(moveDirection))
                .findAny()
                .ifPresentOrElse(
                        moveDirectionObject -> ((Map<String, String>) moveDirectionObject.getValue())
                            .entrySet()
                            .forEach(motorDirection -> broker.publish(
                                    motorTopics.getString(motorDirection.getKey()),
                                    new JSONObject(Map.of(
                                            "speed", speed,
                                            "direction", motorDirection.getValue().toUpperCase()
                                    )),
                                    Priority.NORMAL
                            )),
                        () -> Logger.warn("unknown manual move direction (case ignored): {}", moveDirection));
    }
    
    private void stopMove() {
        makeMove(
                moveDirections
                        .entrySet()
                        .iterator()
                        .next()
                        .getKey(),
                0
        );
    }
    
    @Override
    protected void setup() throws InterruptedException {
        loaded = false;
        broker.subscribe(moveTopic, this);
        broker.subscribe(loadTopic, this);
        broker.subscribe(unloadTopic, this);
        broker.subscribe(clientDisconnectedTopic, this);
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();

        if (loaded || message.topic().equals(loadTopic)) {
            if (message.topic().equals(loadTopic)) {
                loaded = true;
                Logger.info("manual control loaded");
            } else if (message.topic().equals(unloadTopic)) {
                loaded = false;
                stopMove();
                Logger.info("manual control unloaded");
            } else if (message.topic().equals(moveTopic)) {
                String direction = message.payload().getString("direction");
                int speed = message.payload().getInt("speed");
                
                makeMove(direction, speed);
            } else if (message.topic().equals(clientDisconnectedTopic)) {
                stopMove();
                loaded = false;
            }
        }
    }

    public ManualControlModule() {
        super("ManualControlModule");
    }
}
