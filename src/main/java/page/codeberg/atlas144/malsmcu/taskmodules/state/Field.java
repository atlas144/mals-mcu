// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import java.time.LocalDateTime;

/**
 *
 * @author atlas144
 */
public record Field(String name, String value, LocalDateTime updated) {}
