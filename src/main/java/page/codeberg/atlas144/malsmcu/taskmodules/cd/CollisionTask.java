// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd;

import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class CollisionTask extends TimerTask {
    
    private final AtomicBoolean inCollision;
    private final MalsIntermoduleBroker broker;
    
    public CollisionTask(AtomicBoolean inCollision, MalsIntermoduleBroker broker) {
        this.inCollision = inCollision;
        this.broker = broker;
    }

    @Override
    public void run() {
        broker.publish("/command/cd/1/collision/front", "0", Priority.IMPORTANT);
        inCollision.set(false);
    }
    
}
