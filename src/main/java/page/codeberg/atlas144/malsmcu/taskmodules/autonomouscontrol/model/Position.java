// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model;

/**
 *
 * @author atlas144
 */
public class Position extends Point {
    private final double angle;

    public Position(long x, long y, double angle) {
        super(x, y);
        this.angle = angle;
    }

    public Position(double lat, double lon, PhysicalPoint startPoint, double angle) {
        super(lat, lon, startPoint);
        this.angle = angle;
    }

    public double getAngle() {
        return angle;
    }
}
