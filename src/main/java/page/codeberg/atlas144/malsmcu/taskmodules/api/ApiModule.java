// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.api;

import io.javalin.Javalin;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;

/**
 *
 * @author atlas144
 */
public class ApiModule extends TaskModule {
    private static final int PORT = 8080;
    
    private final Javalin api;
    private final ControlService controlService;
    private final LtService ltService;
    private final Controller controller;
    
    public ApiModule() {
        super("ApiModule");
        api = Javalin.create().start(PORT);
        controlService = new ControlService(broker);
        ltService = new LtService(broker);
        controller = new Controller(api, controlService, ltService);
    }
    
    @Override
    protected void setup() {}

    @Override
    protected void loop() throws InterruptedException {}
}
