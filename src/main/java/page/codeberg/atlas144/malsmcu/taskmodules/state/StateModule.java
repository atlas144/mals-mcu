// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import com.moandjiezana.toml.Toml;
import java.sql.SQLException;
import java.util.ArrayList;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class StateModule extends TaskModule {
    private final Toml topics = MibTopics.getInsance().getToml().getTable("state");
    
    private final String PERSISTENT_SET = topics.getString("persistentSet");
    private final String OPERATION_SET = topics.getString("operationSet");
    private final String PERSISTENT_GET = topics.getString("persistentGet");
    private final String OPERATION_GET = topics.getString("operationGet");
    
    private StorageService storageService;
    
    public StateModule() {
        super("StateModule");
        
        try {
            storageService = SqliteStorageServiceInjector.getStorageService();
        } catch (SQLException ex) {
            Logger.error("State DB initialization failed: {}", ex.getMessage());
        }
    }

    @Override
    protected void setup() throws InterruptedException {
        broker.subscribe(new ArrayList(topics.toMap().values()), this);
        
        try {
            storageService.clearOperationStorage();
        } catch (Exception ex) {
            Logger.warn("Operation DB clearing failed!");
        }
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        String fieldName = message.payload().getString("name");
        
        try {
            if (message.topic().equals(PERSISTENT_SET)) {
                String value = message.payload().getString("value");

                storageService.setPersistentField(fieldName, value);
            }
            else if (message.topic().equals(OPERATION_SET)) {
                String value = message.payload().getString("value");

                storageService.setOperationField(fieldName, value);
            }
            else if (message.topic().equals(PERSISTENT_GET)) {
                Field field = storageService.getPersistentField(fieldName);

                broker.publish(String.format("%s/%s", PERSISTENT_GET, fieldName), field.value(), Priority.NORMAL);
            }
            else if (message.topic().equals(OPERATION_GET)) {
                Field field = storageService.getOperationField(fieldName);

                broker.publish(String.format("%s/%s", OPERATION_GET, fieldName), field.value(), Priority.NORMAL);
            }
        } catch (Exception ex) {
            Logger.error(ex.getMessage());
        }
    }
}
