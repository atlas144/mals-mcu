// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.PhysicalPoint;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Position;

/**
 *
 * @author atlas144
 */
public class LocalizationService {
    private static final double PULSES_PER_TURN = 5;
    private static final double WHEEL_DIAMETER = 26;
    private static final double GEARBOX_RATIO = 1/6;
    private static final double WHEELBASE = 34;
    private static final double CONVERSION_FACTOR = (Math.PI * WHEEL_DIAMETER) / (GEARBOX_RATIO * PULSES_PER_TURN);
    
    private PhysicalPoint startPoint;
    private Position position;
    
    public void updateOdometryPosition(final int fl, final int fr, final int bl, final int br) {
        int leftPulses = (fl + bl) / 2;
        int rightPulses = (fr + br) / 2;
        
        double leftDistance = leftPulses * CONVERSION_FACTOR;
        double rightDistance = rightPulses * CONVERSION_FACTOR;
        double centerDistance = (leftDistance + rightDistance) / 2;
        
        double angleDifference = (rightDistance - leftDistance) / WHEELBASE;
        double newAngle = position.getAngle() + angleDifference;
        
        long x = (long) (position.getX() + centerDistance * Math.cos(newAngle));
        long y = (long) (position.getY() + centerDistance * Math.sin(newAngle));
        
        position = new Position(x, y, newAngle);
    }
    
    public void setGpsPosition(final double lat, final double lon) {
        position = new Position(lat, lon, startPoint, 0);
    }
    
    public void setStartPoint(PhysicalPoint startPoint) {
        this.startPoint = startPoint;
        position = new Position(0, 0, 0);
    }
    
    public Position getPosition() {
        return position;
    }
}
