// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol;

import page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model.Move;
import page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model.Symbol;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.tinylog.Logger;

/**
 *
 * @author atlas144
 */
public class LineReader extends Thread {
    private static final String IMAGE_FILE = "/home/mals/Pictures/lt.jpg";
    
    private static final Scalar LOWER_BOUNDARY = new Scalar(5, 5, 5);
    private static final Scalar UPPER_BOUNDARY = new Scalar(0, 0, 0);
    private static final Scalar RED_LOW = new Scalar(0, 70, 75);
    private static final Scalar RED_UP = new Scalar(0, 100, 100);
    private static final Scalar GREEN_LOW = new Scalar(60, 60, 60);
    private static final Scalar GREEN_UP = new Scalar(60, 100, 100);
    private static final Scalar BLUE_LOW = new Scalar(120, 50, 60);
    private static final Scalar BLUE_UP = new Scalar(120, 100, 100);
    
    private final Semaphore running;
    private final SymbolStatus status;
    private final ImageVisualizer visualizer;
    private final Consumer<Move> moveHandler;
    private final Consumer<Symbol> symbolHandler;
    private final Consumer<String> visualizationHandler;
        
    public LineReader(Consumer<Move> moveHandler, Consumer<Symbol> symbolHandler, Consumer<String> visualizationHandler) {
        this.moveHandler = moveHandler;
        this.symbolHandler = symbolHandler;
        this.visualizationHandler = visualizationHandler;
        status = new SymbolStatus();
        visualizer = new ImageVisualizer();
        running = new Semaphore(1);
        running.acquireUninterruptibly();
    }
    
    private boolean captureImage(String absoluteImagePath) {
        try {
            Process captureProcess = new ProcessBuilder("rpicam-still", "-o", absoluteImagePath, "-v", "1", "-t", "10", "-n", "--width", "800", "--height", "800", "-q", "80").start();
            
            captureProcess.waitFor();
            
            return captureProcess.exitValue() == 0;
        } catch (IOException e) {
            Logger.error("image capture command execution failed, error: {}", e.getMessage());
        } catch (InterruptedException e) {
            Logger.info("image capture command execution interrupted, error: {}", e.getMessage());
        }
        
        return false;
    }
    
    private byte[] loadImage(String absoluteImagePath) throws IOException {
        return FileUtils.readFileToByteArray(new File(absoluteImagePath));
    }
    
    private Mat buildMat(byte[] data) {
        return Imgcodecs.imdecode(new MatOfByte(data), Imgcodecs.IMREAD_COLOR);
    }
    
    private MatOfPoint getMax(List<MatOfPoint> contours) {
        MatOfPoint max = contours.get(0);
        double area = Imgproc.contourArea(max);
        
        for (int i = 1; i < contours.size(); ++i) {
            MatOfPoint contour = contours.get(i);
            double currentArea = Imgproc.contourArea(contour);
            
            if (currentArea > area) {
                max = contour;
                area = currentArea;
            }
        }
        
        return max;
    }
    
    private Point countCenter(Moments moments) throws Exception {
        if (moments.m00 != 0) {
            double x = moments.m10 / moments.m00;
            double y = moments.m01 / moments.m00;
            
            return new Point(x, y);
        }
        
        throw new Exception();
    }
    
    private void processPath(MatOfPoint path) {
        try {
            Moments moments = Imgproc.moments(path);
            Point center = countCenter(moments);

            if (center.x <= 40) {
                moveHandler.accept(Move.RIGHT);
            } else if (center.x <= 65) {
                moveHandler.accept(Move.SOFT_RIGHT);
            } else if (center.x <= 95) {
                moveHandler.accept(Move.FORWARD);
            } else if (center.x <= 120) {
                moveHandler.accept(Move.SOFT_LEFT);
            } else {
                moveHandler.accept(Move.LEFT);
            }
        } catch(Exception e) {}
    }
    
    private Symbol findSymbol(Mat image) {
        Symbol symbol = Symbol.NONE;

        if (checkForSymbol(image, RED_LOW, RED_UP, Symbol.RED.color)) {
            symbol = Symbol.RED;
        } else if (checkForSymbol(image, GREEN_LOW, GREEN_UP, Symbol.GREEN.color)) {
            symbol = Symbol.GREEN;
        } else if (checkForSymbol(image, BLUE_LOW, BLUE_UP, Symbol.BLUE.color)) {
            symbol = Symbol.BLUE;
        }
        
        return symbol;
    }
    
    private boolean checkForSymbol(Mat image, Scalar lowerBoundary, Scalar upperBoundary, Scalar color) {
        Mat circles = new Mat();
        Mat copy = image.clone();
        
        Imgproc.cvtColor(copy, copy, Imgproc.COLOR_BGR2HSV);
        Core.inRange(copy, lowerBoundary, upperBoundary, copy);
        Imgproc.HoughCircles(copy, circles, Imgproc.HOUGH_GRADIENT, 1, (double) copy.rows() / 16, 100, 30, 1);
        
        visualizer.drawSymbol(color, circles);
        
        return circles.cols() > 0;
    }

    private void processSymbool(Symbol symbol) {
        status.imageWithSymbol();
                    
        if (status.postSymbolDelayPassed()) {
            moveHandler.accept(Move.STOP);
            status.symbolProcessing();
            symbolHandler.accept(symbol);
        }
    }
    
    @Override
    public void run() {        
        try {
            while (true) {
                block();
                captureImage(IMAGE_FILE);

                Mat origin = buildMat(loadImage(IMAGE_FILE));
                Symbol symbol = findSymbol(origin);
                
                visualizer.setImage(origin);

                if (symbol != Symbol.NONE) {
                    Logger.debug("{} symbol found", symbol.toString());
                    processSymbool(symbol);
                }

                if (!status.isSymbolInProcess()) {
                    Mat biColor = origin.clone();
                    List<MatOfPoint> contours = new ArrayList<>();

                    Core.inRange(origin, LOWER_BOUNDARY, UPPER_BOUNDARY, biColor);
                    Imgproc.findContours(biColor, contours, new Mat(), 1, Imgproc.CHAIN_APPROX_NONE);

                    if (!contours.isEmpty()) {
                        MatOfPoint path = getMax(contours);
                        
                        visualizer.drawPath(path);
                        processPath(path);
                    } else {
                        moveHandler.accept(Move.STOP);
                    }
                }

                visualizationHandler.accept(visualizer.getImageBase64());
                Thread.sleep(100);
            }
        } catch(IOException e) {
            Logger.error("LT camera image can't be loaded");
        } catch(InterruptedException e) {
            Logger.info("line reader ends");
        }    
    }
    
    private synchronized void block() throws InterruptedException {
        running.acquire();
        running.release();
    }
    
    public synchronized void startReader() {
        running.release();
    }
    
    public synchronized void stopReader() {
        running.acquireUninterruptibly();
    }
    
    public void symbolProcessingFinished() {
        status.symbolProcessingFinished();
    }
}
