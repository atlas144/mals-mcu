// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd.sonar;

import java.util.LinkedList;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.CdDirection;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.CollisionHandler;

/**
 *
 * @author atlas144
 */
public class SonarCd {
    
    private static final int MIN_NUMBER_OF_MEASUREMENTS = 5;
    
    private final String moduleName;
    private final CollisionHandler handler;
    private final CdDirection cdDirection;
    private final SonarDirection sonarDirection;
    private final int distanceThreshold;
    
    private final LinkedList<Integer> distances;
    
    private void checkForCollision(int distance) {
        if (distance <= distanceThreshold) {
            distances.add(distance);
            
            if (distances.size() >= MIN_NUMBER_OF_MEASUREMENTS) {
                handler.collision();
                Logger.info("{}: SONAR ({} - {}) - collision detected (distance: {})", moduleName, cdDirection.toString(), sonarDirection.toString(), distances.getLast());
                distances.clear();
            }
        } else {
            distances.clear();
        }
    }
    
    public SonarCd(String moduleName, CollisionHandler handler, CdDirection cdDirection, SonarDirection sonarDirection, int distanceThreshold) {
        this.moduleName = moduleName;
        this.handler = handler;
        this.cdDirection = cdDirection;
        this.sonarDirection = sonarDirection;
        this.distanceThreshold = distanceThreshold;
        distances = new LinkedList<>();
    }
    
    public void addDistance(int distance) {
        Logger.debug("{}: SONAR ({} - {}) - new distance: {}", moduleName, cdDirection.toString(), sonarDirection.toString(), distance);
        checkForCollision(distance);
    }
    
}
