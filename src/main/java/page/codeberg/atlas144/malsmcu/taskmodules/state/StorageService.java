// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import org.tinylog.Logger;

/**
 *
 * @author atlas144
 */
public class StorageService {
    private final StateDal state;
    
    public StorageService(StateDal stateDal) {
        state = stateDal;
    }
    
    private Field getField(Storage storage, String name) throws Exception {
        Field field = state.getField(storage, name);
        
        Logger.trace("Fetched {} field '{}', value: '{}'", storage.name, name, field.value());
        
        return field;
    }
    
    public Field getPersistentField(String name) throws Exception {
        return getField(Storage.PERSISTENT, name);
    }
    
    public Field getOperationField(String name) throws Exception {
        return getField(Storage.OPERATION, name);
    }
    
    public boolean checkPersistentField(String name) throws Exception {
        return state.checkField(Storage.PERSISTENT, name);
    }
    
    public boolean checkOperationField(String name) throws Exception {
        return state.checkField(Storage.OPERATION, name);
    }
    
    private void setField(Storage storage, String name, String value) throws Exception {
        if (state.checkField(storage, name)) {
            state.updateField(storage, name, value);
        } else {
            state.createField(storage, name, value);
        }
        
        Logger.trace("{}{} field '{}' set to '{}'", storage.name.substring(0, 1).toUpperCase(), storage.name.substring(1), name, value);
    }
    
    public void setPersistentField(String name, String value) throws Exception {
        setField(Storage.PERSISTENT, name, value);
    }
    
    public void setOperationField(String name, String value) throws Exception {
        setField(Storage.OPERATION, name, value);
    }
    
    public void clearOperationStorage() throws Exception {
        state.clearOperationStorage();
        Logger.debug("Operation DB cleared");
    }
}
