// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

/**
 *
 * @author atlas144
 */
public interface StateDal {
    Field getField(Storage storage, String name) throws Exception;
    boolean checkField(Storage storage, String name) throws Exception;
    void updateField(Storage storage, String name, String value) throws Exception;
    void createField(Storage storage, String name, String value) throws Exception;
    void clearOperationStorage() throws Exception;
}
