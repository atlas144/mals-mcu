// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import com.pi4j.io.pwm.Pwm;
import com.pi4j.io.pwm.PwmType;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsmcu.model.MotorDirection;

/**
 *
 * @author atlas144
 */
public class Motor {
    private static final int FREQUENCY = 490;
    private static final String PROVIDER = "pigpio-pwm";
    
    private final String id;
    private final Pwm forward;
    private final Pwm backward;

    public Motor(Context context, String id, int fwPin, int bwPin) {
        this.id = id;
        forward = context.create(Pwm.newConfigBuilder(context)
            .address(fwPin)
            .pwmType(PwmType.SOFTWARE)
            .provider(PROVIDER)
            .initial(0)
            .shutdown(0)
            .build()
        );
        backward = context.create(Pwm.newConfigBuilder(context)
            .address(bwPin)
            .pwmType(PwmType.SOFTWARE)
            .provider(PROVIDER)
            .initial(0)
            .shutdown(0)
            .build()
        );
    }
    
    public void move(MotorDirection direction, int dutyCycle) {
        if (direction.equals(MotorDirection.BACKWARD)) {
            forward.off();
            backward.on(dutyCycle, FREQUENCY);
        } else {
            backward.off();
            forward.on(dutyCycle, FREQUENCY);
        }
        
        Logger.debug("motor {} - moved {} (dc {} %)", id, direction.toString().toLowerCase(), dutyCycle);
    }
}
