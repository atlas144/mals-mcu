// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal;

import com.moandjiezana.toml.Toml;
import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import java.util.Map;
import java.util.stream.Collectors;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsmcu.config.MibTopics;
import page.codeberg.atlas144.malsmcu.config.hardware.Halls;
import page.codeberg.atlas144.malsmcu.config.hardware.Lidars;
import page.codeberg.atlas144.malsmcu.config.hardware.Motors;
import page.codeberg.atlas144.malsmcu.config.hardware.Ssas;
import page.codeberg.atlas144.malsmcu.config.hardware.Tactiles;
import page.codeberg.atlas144.malsmcu.model.MotorDirection;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Hall;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Lidar;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Motor;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Ssa;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Tactile;

/**
 *
 * @author atlas144
 */
public class HalModule extends TaskModule {
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml scuTopics = topics.getTable("scu");
    private final Toml motorTopics = scuTopics.getTable("motors");
    private final Toml nonTwiSensors = scuTopics.getTable("nonTwiSensors");
    private final Toml twiSensors = scuTopics.getTable("twiSensors");
    
    private final DeviceFactory deviceFactory;
    private final Context context;
    
    private final Toml motorsToml;
    private final Toml hallsToml;
    private final Toml tactilesToml;
    private final Toml lidarsToml;
    private final Toml ssasToml;
    
    private final Map<String, String> motorTopicsMap;
    
    private final Map<String, Motor> motors;
    private final Map<String, Hall> halls;
    private final Map<String, Tactile> tactiles;
    private final Map<String, Lidar> lidars;
    private final Map<String, Ssa> ssas;
    
    public HalModule() {
        super("HalModule");
        
        context = Pi4J.newAutoContext();
        deviceFactory = new DeviceFactory(context);
        
        motorsToml = Motors.getInsance().getToml();
        hallsToml = Halls.getInsance().getToml();
        tactilesToml = Tactiles.getInsance().getToml();
        lidarsToml = Lidars.getInsance().getToml();
        ssasToml = Ssas.getInsance().getToml();
        
        motorTopicsMap = motorTopics
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(), 
                        entry -> (String) entry.getValue()
                ));
        
        motors = deviceFactory.buildMotors(motorsToml);
        halls = deviceFactory.buildHalls(broker, nonTwiSensors, hallsToml);
        tactiles = deviceFactory.buildTactiles(broker, nonTwiSensors, tactilesToml);
        lidars = deviceFactory.buildLidars(broker, twiSensors, lidarsToml);
        ssas = deviceFactory.buildSsa(broker, twiSensors, ssasToml);
    }
    
    @Override
    protected void setup() throws InterruptedException {
        lidars.values().forEach(lidar -> lidar.start());
        ssas.values().forEach(ssa -> ssa.start());
        
        broker.subscribe(motorTopicsMap.values().stream().toList(), this);
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        
        motorTopicsMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(message.topic()))
                .findAny()
                .ifPresent(entry -> {
                    if (motors.containsKey(entry.getKey())) {
                        motors.get(entry.getKey()).move(
                                message.payload().getEnum(MotorDirection.class, "direction"),
                                message.payload().getInt("speed")
                        );
                    }
                });
    }
}
