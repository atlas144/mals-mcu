// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.api;

import com.moandjiezana.toml.Toml;
import io.javalin.http.Context;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class ControlService {
    private final MalsIntermoduleBroker broker;
    private final Toml topics;
    
    public ControlService(MalsIntermoduleBroker broker) {
        this.broker = broker;
        topics = MibTopics.getInsance().getToml().getTable("client");
    }
    
    public void manualLoad(Context context) {
        broker.publish(topics.getString("ltUnload"), Priority.NORMAL);
        broker.publish(topics.getString("manualLoad"), Priority.NORMAL);
        Logger.info("load manual control request accepted");
        context.status(200);
    }
    
    public void ltLoad(Context context) {
        broker.publish(topics.getString("manualUnload"), Priority.NORMAL);
        broker.publish(topics.getString("ltLoad"), Priority.NORMAL);
        Logger.info("load LT control request accepted");
        context.status(200);
    }
}
