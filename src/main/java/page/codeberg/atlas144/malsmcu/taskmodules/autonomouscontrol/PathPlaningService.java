// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Point;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Position;

/**
 *
 * @author atlas144
 */
public class PathPlaningService {
    public double countDistance(Point currentLocation, Point destination) {
        double x = destination.getX() - currentLocation.getX();
        double y = destination.getY() - currentLocation.getY();
        
        return Math.sqrt(x * x + y * y);
    }
    
    public double countAngle(Position currentLocation, Point destination) throws Exception {
        double x = destination.getX() - currentLocation.getX();
        double y = destination.getY() - currentLocation.getY();
        
        if (x != 0) {
            double desiredAngle = Math.atan(y / x);
            
            return desiredAngle - currentLocation.getAngle();
        } else throw new Exception();
    }
}
