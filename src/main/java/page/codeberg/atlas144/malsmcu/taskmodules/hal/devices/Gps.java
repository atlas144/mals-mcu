// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Gps extends TwiDevice {
    private static final int DATA_SIZE = 12;
    private static final int MEASUREMENT_PERIOD = 1000;
    
    private final String topic;
    
    public Gps(Context context, MalsIntermoduleBroker broker, int address, String topic) {
        super(context, broker, address, DATA_SIZE, MEASUREMENT_PERIOD);
        
        this.topic = topic;
    }
    
    private int binaryToInt(byte[] binary) {
        int intBits = 0, multiplier = 1;
        
        for (byte binaryValue : binary) {
            intBits += binaryValue * multiplier;
            multiplier *= 256;
        }
        
        return intBits;
    }
    
    private float binaryToFloat(byte[] binary) {
        return Float.intBitsToFloat(binaryToInt(binary));
    }
    
    @Override
    protected void processData(byte[] data) {
        JSONObject jsonPayload = new JSONObject(Map.of(
                "lat", binaryToFloat(Arrays.copyOfRange(data, 0, 3)),
                "lon", binaryToFloat(Arrays.copyOfRange(data, 4, 7)),
                "hdop", binaryToInt(Arrays.copyOfRange(data, 8, 11))
        ));
        
        broker.publish(topic, jsonPayload, Priority.NORMAL);
    }    
}
