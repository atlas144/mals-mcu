// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd;

/**
 *
 * @author atlas144
 */
public enum CdDirection {

    FRONT,
    BACK
    
}
