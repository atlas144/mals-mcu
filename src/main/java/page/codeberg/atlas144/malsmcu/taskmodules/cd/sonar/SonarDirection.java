// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd.sonar;

/**
 *
 * @author atlas144
 */
public enum SonarDirection {

    FACE,
    BOTTOM
    
}
