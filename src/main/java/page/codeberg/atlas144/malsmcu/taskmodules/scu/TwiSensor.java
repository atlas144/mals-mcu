// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.scu;

import java.util.Map;

/**
 *
 * @author atlas144
 */
public class TwiSensor extends Device {
    private final short twiAddress;
    private final int tasksDuration;

    public TwiSensor(long srscPacketType, int twiAddress, int dataSize, DeviceType deviceType, int tasksDuration, Map<String, String> mibTopics) {
        super(srscPacketType, dataSize, deviceType, mibTopics);
        this.twiAddress = (short) twiAddress;
        this.tasksDuration = tasksDuration;
    }

    public short getTwiAddress() {
        return twiAddress;
    }

    public int getTasksDuration() {
        return tasksDuration;
    }
    
    public short[] buildRegistrationPayload() {
        short[] payload = new short[5];
        
        payload[0] = getSrscPacketType();
        payload[1] = twiAddress;
        payload[2] = getDataSize();
        payload[3] = (short) (tasksDuration & 0b11111111);
        payload[4] = (short) (tasksDuration >> 8);
        
        return payload;
    }
}
