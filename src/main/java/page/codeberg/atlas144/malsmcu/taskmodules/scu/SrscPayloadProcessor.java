// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.scu;

import java.util.Arrays;
import java.util.Map;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class SrscPayloadProcessor {
    private final MalsIntermoduleBroker broker;

    public SrscPayloadProcessor(MalsIntermoduleBroker broker) {
        this.broker = broker;
    }
    
    public void processLidarPayload(short[] payload, String mibTopic) {
        short angle = payload[0];
        int distance = payload[1] + 256 * payload[2];
        JSONObject jsonPayload = new JSONObject(Map.of(
                "angle", angle,
                "distance", distance
        ));
        
        broker.publish(mibTopic, jsonPayload, Priority.NORMAL);
    }
    
    public void processSsaPayload(short[] payload, String mibFrontTopic, String mibBottomTopic) {
        short frontDistance = payload[0];
        short bottomDistance = payload[1];
        
        broker.publish(mibFrontTopic, frontDistance, Priority.NORMAL);
        broker.publish(mibBottomTopic, bottomDistance, Priority.NORMAL);
    }
    
    public void processTactilePayload(String mibTopic) {
        broker.publish(mibTopic, Priority.CRITICAL);
    }
    
    public void processLtsPayload(short[] payload, String mibTopic) {
        JSONObject jsonPayload = new JSONObject(Map.of(
                "left", payload[0],
                "center", payload[1],
                "right", payload[2]
        ));
        
        broker.publish(mibTopic, jsonPayload, Priority.NORMAL);
    }
    
    private int binaryToInt(short[] binary) {
        int intBits = 0, multiplier = 1;
        
        for (short binaryValue : binary) {
            intBits += binaryValue * multiplier;
            multiplier *= 256;
        }
        
        return intBits;
    }
    
    private float binaryToFloat(short[] binary) {
        return Float.intBitsToFloat(binaryToInt(binary));
    }
    
    public void processGpsPayload(short[] payload, String mibTopic) {
        JSONObject jsonPayload = new JSONObject(Map.of(
                "lat", binaryToFloat(Arrays.copyOfRange(payload, 0, 3)),
                "lon", binaryToFloat(Arrays.copyOfRange(payload, 4, 7)),
                "hdop", binaryToInt(Arrays.copyOfRange(payload, 8, 11))
        ));
        
        broker.publish(mibTopic, jsonPayload, Priority.NORMAL);
    }
    
    public void processHallPayload(short[] payload, String flTopic, String frTopic, String blTopic, String brTopic) {
        broker.publish(flTopic, payload[0], Priority.NORMAL);
        broker.publish(frTopic, payload[1], Priority.NORMAL);
        broker.publish(blTopic, payload[2], Priority.NORMAL);
        broker.publish(brTopic, payload[3], Priority.NORMAL);
    }
}
