// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.io.gpio.digital.PullResistance;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Hall {
    private static final String PROVIDER = "pigpio-digital-input";
    
    private final MalsIntermoduleBroker broker;
    private final DigitalInput hall;
    private final String topic;

    public Hall(Context context, MalsIntermoduleBroker broker, int pin, String topic) {
        this.broker = broker;
        this.topic = topic;
        hall = context.create(DigitalInput.newConfigBuilder(context)
            .address(pin)
            .provider(PROVIDER)
            .pull(PullResistance.PULL_DOWN)
            .build()
        );
        
        hall.addListener(e -> {
            if (e.state() == DigitalState.HIGH) {
                precessChange();
            }
        });
    }
    
    private void precessChange() {
        broker.publish(topic, 1, Priority.NORMAL);
    }
}
