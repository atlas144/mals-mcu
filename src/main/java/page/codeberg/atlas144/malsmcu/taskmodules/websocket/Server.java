// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.websocket;

import com.moandjiezana.toml.Toml;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import org.java_websocket.WebSocket;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class Server extends WebSocketServer {
    private final Toml topics = MibTopics.getInsance().getToml();
    private final String host;
    private final short port;
    private final MalsIntermoduleBroker broker;
    
    private WebSocket connection;
    private OnMessageListener onMessageListener;
    
    public Server(String host, short port, MalsIntermoduleBroker broker) {
        super(new InetSocketAddress(host, port));
        
        this.host = host;
        this.port = port;
        this.broker = broker;
        connection = null;
        Logger.info("WebSocket server successfully created");
    }

    @Override
    public void onOpen(WebSocket connection, ClientHandshake clientHandshake) {
        if (this.connection == null) {
            this.connection = connection;
            broker.publish(topics.getString("client.connected"), Priority.NORMAL);
            Logger.info("connection '{}' successfully opened", connection.getRemoteSocketAddress().toString());
        } else {
            connection.close(CloseFrame.TRY_AGAIN_LATER, "another client already connected");
            Logger.info("connection attempt from '{}' while another connection already opened", connection.getRemoteSocketAddress().toString());
        }
    }

    @Override
    public void onClose(WebSocket connection, int code, String reason, boolean initiatedByRemote) {
        if (connection.equals(this.connection)) {
            this.connection = null;
            Logger.info("connection close request recieved, waiting for reconnect");
            
            new Thread(() -> {
                try {
                    Thread.sleep(1500);
                    
                    if (this.connection == null) {
                        broker.publish(topics.getString("client.disconnected"), Priority.NORMAL);
                        Logger.info("connection '{}' successfully closed (reason: '{}', code: {}, remote: {})", connection.getRemoteSocketAddress().toString(), reason, code, initiatedByRemote);
                    }
                } catch (InterruptedException ex) {}
            }).start();
        }
    }

    @Override
    public void onMessage(WebSocket connection, String message) {
        onMessageListener.onMessage(connection, message);
    }

    @Override
    public void onError(WebSocket connection, Exception e) {
        Logger.warn("error occurred on connection: '{}', error: {}", connection.getRemoteSocketAddress().toString(), e.getMessage());
    }

    @Override
    public void onStart() {
        Logger.info("WebSocket server started successfully on '{}:{}'", host, port);
    }
    
    public void registerOnMessageListener(OnMessageListener listener) {
        onMessageListener = listener;
    }
    
    public void sendMessage(String messageJson) {
        connection.send(messageJson);
    }
    
    public boolean isConnected() {
        return connection != null && connection.isOpen();
    }
}
