// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.websocket;

import org.java_websocket.WebSocket;

/**
 *
 * @author atlas144
 */
public interface OnMessageListener {
    void onMessage(WebSocket connection, String messageJson);
}
