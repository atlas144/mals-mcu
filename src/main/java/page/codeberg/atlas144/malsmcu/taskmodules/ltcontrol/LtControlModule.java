// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol;

import page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model.Move;
import page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model.Symbol;
import com.moandjiezana.toml.Toml;
import java.util.List;
import java.util.Map;
import nu.pattern.OpenCV;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class LtControlModule extends TaskModule {
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml ltTopics = topics.getTable("lt");
    private final String loadTopic = topics.getString("client.ltLoad");
    private final String unloadTopic = topics.getString("client.ltUnload");
    private final String clientDisconnectedTopic = topics.getString("client.disconnected");
    private final String speedTopic = ltTopics.getString("speed");
    private final String startTopic = ltTopics.getString("start");
    private final String stopTopic = ltTopics.getString("stop");
    private final String symbolProcessingFinishedTopic = ltTopics.getString("symbolProcessingFinished");
    private final String visualizationTopic = ltTopics.getString("visualization");
    
    private final List<String> motorTopics;
    private final LineReader lineReader;
    
    private boolean loaded;
    private int speedModifier;

    public LtControlModule() {
        super("LtsModule");
        motorTopics = topics.getTable("scu.motors").toMap().values().stream().map(topic -> (String) topic).toList();
        lineReader = new LineReader(this::makeMove, this::reactToSymbol, this::sendVisualizationImage);
        speedModifier = 0;
    }

    @Override
    protected void setup() throws InterruptedException {
        loaded = false;
        broker.subscribe(speedTopic, this);
        broker.subscribe(startTopic, this);
        broker.subscribe(stopTopic, this);
        broker.subscribe(symbolProcessingFinishedTopic, this);
        broker.subscribe(loadTopic, this);
        broker.subscribe(unloadTopic, this);
        broker.subscribe(clientDisconnectedTopic, this);
        
        OpenCV.loadShared();
        
        lineReader.start();
    }

    @Override
    protected void loop() throws InterruptedException {
        Message message = messageQueue.take();
        
        if (loaded || message.topic().equals(loadTopic)) {
            if (message.topic().equals(speedTopic)) {
                speedModifier = message.payload().getInt("value");
            } else if (message.topic().equals(startTopic)) {
                lineReader.startReader();
            } else if (message.topic().equals(stopTopic)) {
                lineReader.stopReader();

                makeMove(Move.STOP);
            } else if (message.topic().equals(symbolProcessingFinishedTopic)) {
                lineReader.symbolProcessingFinished();
            } else if (message.topic().equals(loadTopic)) {
                loaded = true;
            } else if (message.topic().equals(unloadTopic)) {
                loaded = false;
                
                stopVehicle();
            } else if (message.topic().equals(clientDisconnectedTopic)) {
                loaded = false;
                
                stopVehicle();
            }
        }
    }
    
    private void stopVehicle() {
        makeMove(Move.STOP);
        lineReader.stopReader();
    }
    
    private void makeMove(Move move) {
        for (int i = 0; i < motorTopics.size(); ++i) {
            var motorConfig = move.motors[i];
            
            broker.publish(motorTopics.get(i), new JSONObject(Map.of(
                    "direction", motorConfig.getDirection(),
                    "speed", motorConfig.getSpeed() * (speedModifier / 100.0)
            )), Priority.NORMAL);
        }
    }
    
    private void reactToSymbol(Symbol symbol) {
        // Implement calls to the robotic arm API.
    }
    
    private void sendVisualizationImage(String base64Image) {
        broker.publish(visualizationTopic, base64Image, Priority.NORMAL);
    }
}
