// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import java.util.Map;
import page.codeberg.atlas144.malsmcu.model.ComponentPosition;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.AcumulatorCell;

/**
 *
 * @author atlas144
 */
public class HallAcumulator {
    private static final int PROCESS_THRESHOLD = 5;
    
    private final Map<ComponentPosition, AcumulatorCell> cells;
    
    public HallAcumulator() {
        cells = Map.of(
                ComponentPosition.FL, new AcumulatorCell(ComponentPosition.FL),
                ComponentPosition.FR, new AcumulatorCell(ComponentPosition.FR),
                ComponentPosition.BL, new AcumulatorCell(ComponentPosition.BL),
                ComponentPosition.BR, new AcumulatorCell(ComponentPosition.BR)
        );
    }
    
    public void reset() {
        cells.values().forEach(cell -> cell.set(0));
    }
    
    public void increment(ComponentPosition encoderPosition) {
        cells.get(encoderPosition).incrementAndGet();
    }

    public int getValue(ComponentPosition encoderPosition) {
        return cells.get(encoderPosition).get();
    }
    
    public boolean isReady() {
        return cells
                .values()
                .stream()
                .filter(cell -> cell.get() >= PROCESS_THRESHOLD)
                .findAny()
                .isPresent();
    }
}
