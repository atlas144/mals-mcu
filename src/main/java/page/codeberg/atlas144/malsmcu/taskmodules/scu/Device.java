// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.scu;

import java.util.Map;

/**
 *
 * @author atlas144
 */
public class Device {
    private final short srscPacketType;
    private final short dataSize;
    private final DeviceType deviceType;
    private final Map<String, String> mibTopics;

    public Device(long srscPacketType, int dataSize, DeviceType deviceType, Map<String, String> mibTopics) {
        this.srscPacketType = (short) srscPacketType;
        this.dataSize = (short) dataSize;
        this.deviceType = deviceType;
        this.mibTopics = mibTopics;
    }

    public short getSrscPacketType() {
        return srscPacketType;
    }

    public short getDataSize() {
        return dataSize;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public String getMibTopic(String topicIdentifier) {
        return mibTopics.get(topicIdentifier);
    }

    public Map<String, String> getMibTopics() {
        return mibTopics;
    }
}
