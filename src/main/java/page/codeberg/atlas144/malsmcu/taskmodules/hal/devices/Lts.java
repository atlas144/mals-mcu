// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import java.util.Map;
import org.json.JSONObject;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Lts extends TwiDevice {
    private static final int DATA_SIZE = 3;
    private static final int MEASUREMENT_PERIOD = 60;
    
    private final String topic;
    
    public Lts(Context context, MalsIntermoduleBroker broker, int address, String topic) {
        super(context, broker, address, DATA_SIZE, MEASUREMENT_PERIOD);
        
        this.topic = topic;
    }
    
    @Override
    protected void processData(byte[] data) {
        JSONObject jsonPayload = new JSONObject(Map.of(
                "left", data[0],
                "center", data[1],
                "right", data[2]
        ));
        
        broker.publish(topic, jsonPayload, Priority.NORMAL);
    }
}
