// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import java.io.InputStream;
import net.sourceforge.jFuzzyLogic.FIS;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.MotorControlResult;

/**
 *
 * @author atlas144
 */
public class FuzzyControllerService {
    private static final String FCL_PATH = "fuzzy/autonomous_control.fcl";
    private static final String DISTANCE = "distance";
    private static final String ANGLE = "angle";
    private static final String LEFT_SPEED = "leftSpeed";
    private static final String RIGHT_SPEED = "rightSpeed";
    
    private final FIS fis;
    
    public FuzzyControllerService() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classloader.getResourceAsStream(FCL_PATH);
        
        fis = FIS.load(stream, false);
    }
    
    public MotorControlResult countSpeeds(double distance, double angle) {
        fis.setVariable(DISTANCE, distance);
        fis.setVariable(ANGLE, angle);
        fis.evaluate();

        return new MotorControlResult(
                fis.getVariable(LEFT_SPEED).getValue(),
                fis.getVariable(RIGHT_SPEED).getValue()
        );
    }
}
