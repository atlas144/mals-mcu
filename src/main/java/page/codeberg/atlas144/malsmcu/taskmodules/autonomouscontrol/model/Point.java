// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model;

/**
 *
 * @author atlas144
 */
public class Point {
    private static final double DEGREE_LENGTH = 11111111.111;
    private static final Point START_POINT = new Point(0, 0);
    
    private final long x;
    private final long y;
    
    public Point(long x, long y) {
        this.x = x;
        this.y = y;
    }
    
    public Point(double lat, double lon, PhysicalPoint startPoint) {
        x = (long) ((lat - startPoint.lat()) * DEGREE_LENGTH);
        y = (long) (((lon - startPoint.lon()) * Math.cos(startPoint.lat())) * DEGREE_LENGTH);
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }
    
    public static Point getStartPoint() {
        return START_POINT;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.x ^ (this.x >>> 32));
        hash = 29 * hash + (int) (this.y ^ (this.y >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point other = (Point) obj;
        if (this.x != other.x) {
            return false;
        }
        return this.y == other.y;
    }
}