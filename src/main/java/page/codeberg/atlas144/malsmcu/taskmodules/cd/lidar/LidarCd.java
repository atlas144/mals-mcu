// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd.lidar;

import java.util.LinkedList;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.CollisionHandler;

/**
 *
 * @author atlas144
 */
public class LidarCd {
    private static final int MIN_NUMBER_OF_MEASUREMENTS = 5;
    
    private final String moduleName;
    private final int distanceThreshold;
    private final CollisionHandler handler;
    
    private final LinkedList<Integer> realDistances;
    
    private int countRealDistance(double distance, double angle) {
        return (int) Math.round(Math.cos(angle) * distance);
    }
    
    private void checkForCollision(int realDistance) {
        if (realDistance <= distanceThreshold) {
            realDistances.add(realDistance);

            if (realDistances.size() >= MIN_NUMBER_OF_MEASUREMENTS) {
                handler.collision();
                Logger.info("{}: LiDAR - collision detected (distance: {})", moduleName, realDistances.getLast());
                realDistances.clear();
            }
        } else {
            realDistances.clear();
        }
    }
    
    public void processNewValue(double distance, double angle) {
        if (distance != 0) {
            int realDistance = countRealDistance(distance, angle);

            checkForCollision(realDistance);
        }
    }
    
    public LidarCd(String moduleName, MalsIntermoduleBroker broker, CollisionHandler handler, int distanceThreshold) {
        this.moduleName = moduleName;
        this.handler = handler;
        this.distanceThreshold = distanceThreshold;
        realDistances = new LinkedList<>();
    }
}
