// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

/**
 *
 * @author atlas144
 */
public class FieldNotFoundException extends Exception {
    public FieldNotFoundException(String name) {
        super(String.format("Field '%s' was not found!", name));
    }
}
