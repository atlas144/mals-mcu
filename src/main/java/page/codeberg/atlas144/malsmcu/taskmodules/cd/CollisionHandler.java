// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd;

import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class CollisionHandler {
    
    private static final long DELAY = 3000;
    
    private final MalsIntermoduleBroker broker;
    private final AtomicBoolean inCollision;
    private final Timer timer;
    
    public CollisionHandler(MalsIntermoduleBroker broker) {
        this.broker = broker;
        inCollision = new AtomicBoolean(false);
        timer = new Timer();
    }
    
    public void collision() {
        if (inCollision.get()) {
            timer.cancel();
        } else {
            broker.publish("/command/cd/1/collision/front", "1", Priority.IMPORTANT);
        }
        
        inCollision.set(true);
        timer.schedule(new CollisionTask(inCollision, broker), DELAY);
    }
    
}
