// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model;

/**
 *
 * @author atlas144
 */
public record PhysicalPoint(double lat, double lon) {}
