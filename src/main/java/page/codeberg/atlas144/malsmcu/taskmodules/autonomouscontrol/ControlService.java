// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import java.util.function.Consumer;
import org.json.JSONObject;
import page.codeberg.atlas144.malsmcu.model.ComponentPosition;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.MotorControlResult;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Point;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Position;

/**
 *
 * @author atlas144
 */
public class ControlService extends Thread {
    private static final int ACCEPTABLE_DISTANCE = 20;
    
    private final HallAcumulator acumulator;
    private final LocalizationService localizationService;
    private final MapService mapService;
    private final PathPlaningService pathPlaningService;
    private final FuzzyControllerService fuzzyControllerService;
    private final Consumer<MotorControlResult> speedChangeHandler;
    
    private boolean running;
    
    public ControlService(Consumer<MotorControlResult> speedChangeHandler) {
        this.speedChangeHandler = speedChangeHandler;
        acumulator = new HallAcumulator();
        localizationService = new LocalizationService();
        mapService = new MapService();
        pathPlaningService = new PathPlaningService();
        fuzzyControllerService = new FuzzyControllerService();
        running = false;
    }
    
    public void startControl() {
        acumulator.reset();
        running = true;
    }
    
    public void stopControl() {
        running = false;
    }
    
    public boolean isRunning() {
        return running;
    }
    
    public void increment(ComponentPosition encoderPosition) {
        acumulator.increment(encoderPosition);
    }
    
    public void setPath(JSONObject pathMessagePayload) {
        mapService.setPath(pathMessagePayload);
    }

    @Override
    public void run() {
        try {
            while (true) {
                if (running) {
                    if (acumulator.isReady()) {
                        localizationService.updateOdometryPosition(
                                acumulator.getValue(ComponentPosition.FL),
                                acumulator.getValue(ComponentPosition.FR),
                                acumulator.getValue(ComponentPosition.BL),
                                acumulator.getValue(ComponentPosition.BR)
                        );
                        acumulator.reset();
                        
                        Position currentPosition = localizationService.getPosition();
                        Point destPosition = mapService.getNextPoint();
                        double distance = pathPlaningService.countDistance(currentPosition, destPosition);
                        double angle = pathPlaningService.countAngle(currentPosition, destPosition);
                        
                        if (distance <= ACCEPTABLE_DISTANCE) {
                            mapService.finishSubgoal();
                            destPosition = mapService.getNextPoint();
                            distance = pathPlaningService.countDistance(currentPosition, destPosition);
                            angle = pathPlaningService.countAngle(currentPosition, destPosition);
                        }
                        
                        MotorControlResult speeds = fuzzyControllerService.countSpeeds(distance, angle);
                        
                        speedChangeHandler.accept(speeds);
                            
                        if (mapService.isFinished()) {
                            running = false;
                            speedChangeHandler.accept(MotorControlResult.STOP);
                        }
                    }
                } else {
                    Thread.sleep(1000);
                }
            }
        } catch (Exception ex) {}
    }
}
