// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.PhysicalPoint;
import page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model.Point;

/**
 *
 * @author atlas144
 */
public class MapService {
    private PhysicalPoint startPoint;
    private List<Point> path;
    private int index;

    public void setPath(final JSONObject pathMessagePayload) {
        JSONArray pathJson = pathMessagePayload.getJSONArray("path");
        startPoint = new PhysicalPoint(
                pathJson.getJSONObject(0).getDouble("lat"),
                pathJson.getJSONObject(0).getDouble("lon")
        );
        path = new ArrayList<>(pathJson.length());
        index = 0;
        path.add(Point.getStartPoint());
        
        for (int i = 1; i < pathJson.length(); ++i) {
            JSONObject pointJson = pathJson.getJSONObject(i);
            
            path.add(new Point(pointJson.getDouble("lat"), pointJson.getDouble("lon"), startPoint));
        }
    }
    
    public Point getLastPoint() {
        return path.get(index);
    }
    
    public Point getNextPoint() {
        return path.get(index + 1);
    }
    
    public void finishSubgoal() {
        if (index < path.size() - 1) {
            ++index;
        }
    }
    
    public boolean isFinished() {
        return index >= path.size() - 1;
    }
}
