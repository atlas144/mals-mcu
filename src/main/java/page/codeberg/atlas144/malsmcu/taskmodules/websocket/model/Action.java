// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.websocket.model;

/**
 *
 * @author atlas144
 */
public enum Action {
    PUBLISH,
    SUBSCRIBE
}
