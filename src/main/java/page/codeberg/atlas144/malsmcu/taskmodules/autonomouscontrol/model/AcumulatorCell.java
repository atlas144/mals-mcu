// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model;

import java.util.concurrent.atomic.AtomicInteger;
import page.codeberg.atlas144.malsmcu.model.ComponentPosition;

/**
 *
 * @author atlas144
 */
public class AcumulatorCell extends AtomicInteger {
    private final ComponentPosition encoderPosition;

    public AcumulatorCell(ComponentPosition encoderPosition) {
        super(0);
        this.encoderPosition = encoderPosition;
    }

    public ComponentPosition getEncoderPosition() {
        return encoderPosition;
    }
}
