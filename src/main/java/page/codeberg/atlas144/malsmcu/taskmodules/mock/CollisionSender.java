// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.mock;

import java.util.Map;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class CollisionSender extends Thread {
    private final MalsIntermoduleBroker broker;
    private final Map<String, String> topics;

    public CollisionSender(MalsIntermoduleBroker broker, Map<String, String> topics) {
        this.broker = broker;
        this.topics = topics;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                for (String topic : topics.values()) {
                    broker.publish(topic, true, Priority.NORMAL);
                    sleep(2000);
                    broker.publish(topic, false, Priority.NORMAL);
                }
            }
        } catch (InterruptedException ex) {}
    }
}
