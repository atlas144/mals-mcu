// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.autonomouscontrol.model;

import page.codeberg.atlas144.malsmcu.model.MotorDirection;

/**
 *
 * @author atlas144
 */
public class MotorControlResult {
    public static final MotorControlResult STOP = new MotorControlResult(0, 0);
    
    private final int leftAbsSpeed;
    private final int rightAbsSpeed;
    private final MotorDirection leftDirection;
    private final MotorDirection rightDirection;
    
    private int countAbsSpeed(double speed) {
        return (int) Math.abs(speed);
    }
    
    private MotorDirection determineDirection(double speed) {
        return speed >= 0 ? MotorDirection.FORWARD : MotorDirection.BACKWARD;
    }
    
    public MotorControlResult(double left, double right) {
        leftAbsSpeed = countAbsSpeed(left);
        rightAbsSpeed = countAbsSpeed(right);
        leftDirection = determineDirection(left);
        rightDirection = determineDirection(right);
    }

    public int getLeftSpeed() {
        return leftAbsSpeed;
    }

    public int getRightSpeed() {
        return rightAbsSpeed;
    }

    public MotorDirection getLeftDirection() {
        return leftDirection;
    }

    public MotorDirection getRightDirection() {
        return rightDirection;
    }
}
