// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import java.sql.SQLException;

/**
 *
 * @author atlas144
 */
public class SqliteStorageServiceInjector {
    public static StorageService getStorageService() throws SQLException {
        return new StorageService(
            SqliteStateDal.getInstance()
        );
    }
}
