// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.model;

import page.codeberg.atlas144.malsmcu.model.MotorConfiguration;
import page.codeberg.atlas144.malsmcu.model.MotorDirection;

/**
 *
 * @author atlas144
 */
public enum Move {
    LEFT(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.BACKWARD, 240),
        new MotorConfiguration(MotorDirection.FORWARD, 240),
        new MotorConfiguration(MotorDirection.BACKWARD, 240),
        new MotorConfiguration(MotorDirection.FORWARD, 240)
    }),
    SOFT_LEFT(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.FORWARD, 60),
        new MotorConfiguration(MotorDirection.FORWARD, 240),
        new MotorConfiguration(MotorDirection.FORWARD, 60),
        new MotorConfiguration(MotorDirection.FORWARD, 240)
    }),
    FORWARD(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 240)
    }),
    SOFT_RIGHT(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.BACKWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.BACKWARD, 240)
    }),
    RIGHT(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 60), 
        new MotorConfiguration(MotorDirection.FORWARD, 240), 
        new MotorConfiguration(MotorDirection.FORWARD, 60)
    }),
    STOP(new MotorConfiguration[]{
        new MotorConfiguration(MotorDirection.FORWARD, 0), 
        new MotorConfiguration(MotorDirection.FORWARD, 0), 
        new MotorConfiguration(MotorDirection.FORWARD, 0), 
        new MotorConfiguration(MotorDirection.FORWARD, 0)
    });

    public final MotorConfiguration[] motors;

    private Move(MotorConfiguration[] motors) {
        this.motors = motors;
    }
}
