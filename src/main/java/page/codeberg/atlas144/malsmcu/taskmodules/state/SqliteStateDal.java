// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

/**
 *
 * @author atlas144
 */
public class SqliteStateDal implements StateDal {
    private static SqliteStateDal instance;
    private Connection connection;
        
    private void connect() throws SQLException {
        connection = DriverManager.getConnection("jdbc:sqlite::resource:db/state.db");
    }
    
    private SqliteStateDal() throws SQLException {
        connect();
    }
    
    public static SqliteStateDal getInstance() throws SQLException {
        if (instance == null) {
            instance = new SqliteStateDal();
        }
        
        return instance;
    }

    @Override
    public Field getField(Storage storage, String name) throws Exception {
        PreparedStatement statement = connection.prepareStatement(String.format("SELECT value, updated FROM %sField WHERE name = ?;", storage.name));

        statement.setString(1, name);
        
        ResultSet resultSet = statement.executeQuery();
        
        if (resultSet.next()) {
            return new Field(
                    name,
                    resultSet.getString("value"),
                    LocalDateTime.parse(resultSet.getString("updated"))
            );
        }
        
        throw new FieldNotFoundException(name);
    }

    @Override
    public boolean checkField(Storage storage, String name) throws Exception {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(String.format("SELECT 1 FROM %sField WHERE name = ?;", storage.name));
        
        return resultSet.next();
    }

    @Override
    public void updateField(Storage storage, String name, String value) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(String.format("UPDATE %sField SET value = ?, updated = ? WHERE name = ?;", storage.name));

        statement.setString(1, value);
        statement.setString(2, LocalDateTime.now().toString());
        statement.setString(3, name);
        statement.executeUpdate();
    }

    @Override
    public void createField(Storage storage, String name, String value) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(String.format("INSERT INTO %sField (name, value, updated) VALUES (?, ?, ?);", storage.name));

        statement.setString(1, name);
        statement.setString(2, value);
        statement.setString(3, LocalDateTime.now().toString());
        statement.executeUpdate();
    }

    @Override
    public void clearOperationStorage() throws Exception {
        Statement statement = connection.createStatement();
        
        statement.executeQuery(String.format("DELETE FROM %sField", Storage.OPERATION.name));
    }
}
