// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.scu;

import com.moandjiezana.toml.Toml;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;
import page.codeberg.atlas144.malsmcu.config.SrscPacketTypes;
/*import page.codeberg.atlas144.srsc.Srsc;
import page.codeberg.atlas144.srsc.entity.PacketType;
import page.codeberg.atlas144.srsc.entity.Severity;
import page.codeberg.atlas144.srsc.exception.NotConnectedException;
import page.codeberg.atlas144.srsc.exception.OponentsBufferFullException;
import page.codeberg.atlas144.srsc.exception.PacketTypeNotRegisteredException;
import page.codeberg.atlas144.srsc.exception.WrongPayloadLengthException;
import page.codeberg.atlas144.srsc.service.conversion.ConversionService;
import page.codeberg.atlas144.srsc.service.conversion.DefaultConversionServiceFactory;*/

/**
 *
 * @author atlas144
 */
public class ScuModule extends TaskModule {
    /*private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml scuTopics = topics.getTable("scu");
    private final Toml motorTopics = scuTopics.getTable("motors");
    private final Toml nonTwiSensorTopics = scuTopics.getTable("nonTwiSensors");
    private final Toml twiSensorTopics = scuTopics.getTable("twiSensors");
    private final Toml packetTypes = SrscPacketTypes.getInsance().getToml();
    private final Toml motorPt = packetTypes.getTable("motor");
    private final Toml onboardPt = packetTypes.getTable("onboardDevice");
    private final Toml twiPt = packetTypes.getTable("twiDevice");
        
    private final Srsc srsc;
    private final ConversionService conversionService;
    private final SrscPayloadProcessor payloadProcessor;
    
    private final Map<String, Device> motors = Map.of(
            motorTopics.getString("speed.fl"), new Device(motorPt.getLong("speed.fl"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("speed.fl"))),
            motorTopics.getString("speed.fr"), new Device(motorPt.getLong("speed.fr"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("speed.fr"))),
            motorTopics.getString("speed.bl"), new Device(motorPt.getLong("speed.bl"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("speed.bl"))),
            motorTopics.getString("speed.br"), new Device(motorPt.getLong("speed.br"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("speed.br"))),
            motorTopics.getString("direction.fl"), new Device(motorPt.getLong("direction.fl"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("direction.fl"))),
            motorTopics.getString("direction.fr"), new Device(motorPt.getLong("direction.fr"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("direction.fr"))),
            motorTopics.getString("direction.bl"), new Device(motorPt.getLong("direction.bl"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("direction.bl"))),
            motorTopics.getString("direction.br"), new Device(motorPt.getLong("direction.br"), 1, DeviceType.MOTOR, Map.of("topic", motorTopics.getString("direction.br")))
    );
    private final Map<Long, Device> nonTwiSensors = Map.of(
            onboardPt.getLong("bumper.f"), new Device(onboardPt.getLong("bumper.f"), 0, DeviceType.TACTILE, Map.of("topic", nonTwiSensorTopics.getString("tactileFront"))),
            onboardPt.getLong("bumper.b"), new Device(onboardPt.getLong("bumper.b"), 0, DeviceType.TACTILE, Map.of("topic", nonTwiSensorTopics.getString("tactileBack"))),
            onboardPt.getLong("hall"), new Device(onboardPt.getLong("hall"), 4, DeviceType.HALL, Map.of("fl", nonTwiSensorTopics.getString("hallFl"), "fr", nonTwiSensorTopics.getString("hallFr"), "bl", nonTwiSensorTopics.getString("hallBl"), "br", nonTwiSensorTopics.getString("hallBr")))
    );
    private final Map<Long, TwiSensor> twiSensors = Map.of(
            twiPt.getLong("lidar"), new TwiSensor(twiPt.getLong("lidar"), 0x22, 3, DeviceType.LIDAR, 20, Map.of("topic", twiSensorTopics.getString("lidar"))),
            twiPt.getLong("ssa.fl"), new TwiSensor(twiPt.getLong("ssa.fl"), 0x23, 2, DeviceType.SSA, 60, Map.of("front", twiSensorTopics.getString("ssaFlf"), "bottom", twiSensorTopics.getString("ssaFlb"))),
            twiPt.getLong("ssa.fr"), new TwiSensor(twiPt.getLong("ssa.fr"), 0x24, 2, DeviceType.SSA, 60, Map.of("front", twiSensorTopics.getString("ssaFrf"), "bottom", twiSensorTopics.getString("ssaFrb"))),
            twiPt.getLong("ssa.bl"), new TwiSensor(twiPt.getLong("ssa.bl"), 0x25, 2, DeviceType.SSA, 60, Map.of("front", twiSensorTopics.getString("ssaBlf"), "bottom", twiSensorTopics.getString("ssaBlb"))),
            twiPt.getLong("ssa.br"), new TwiSensor(twiPt.getLong("ssa.br"), 0x26, 2, DeviceType.SSA, 60, Map.of("front", twiSensorTopics.getString("ssaBrf"), "bottom", twiSensorTopics.getString("ssaBrb"))),
            twiPt.getLong("lts"), new TwiSensor(twiPt.getLong("lts"), 0x21, 3, DeviceType.LTS, 50, Map.of("topic", twiSensorTopics.getString("lts"))),
            twiPt.getLong("gps"), new TwiSensor(twiPt.getLong("gps"), 0x28, 12, DeviceType.GPS, 500, Map.of("topic", twiSensorTopics.getString("gps")))
    );
    
    private void registerSrscPacketTypes() throws InterruptedException {
        srsc.registerPacketType((short) packetTypes.getLong("service.registerSensor").longValue(), Severity.CRITICAL, (short) 5);

        for (Device device : motors.values()) {
            for (String topic : device.getMibTopics().values()) {
                broker.subscribe(topic, this);
            }
            
            srsc.registerPacketType(device.getSrscPacketType(), Severity.NORMAL, device.getDataSize());
        }

        for (Device device : nonTwiSensors.values()) {
            srsc.registerPacketType(device.getSrscPacketType(), Severity.NORMAL, device.getDataSize());
        }

        for (TwiSensor sensor : twiSensors.values()) {
            boolean finished = true;
            
            do {
                srsc.registerPacketType(sensor.getSrscPacketType(), Severity.NORMAL, sensor.getDataSize());

                try {
                    srsc.writePacket((short) packetTypes.getLong("service.registerSensor").longValue(), sensor.buildRegistrationPayload());
                } catch (PacketTypeNotRegisteredException | WrongPayloadLengthException ex) {
                    Logger.error("{}: error: '{}'", getModuleName(), ex.getMessage());
                } catch (NotConnectedException ex) {
                    JSONObject scuDisconnectedPayload = new JSONObject(Map.of(
                            "name", "scuConnected",
                            "value", false)
                    );

                    broker.publish(topics.getString("state.operationSet"), scuDisconnectedPayload, Priority.NORMAL);
                    broker.publish(scuTopics.getString("disconnected"), Priority.NORMAL);
                    
                    Logger.error("{}: error: '{}'", getModuleName(), ex.getMessage());
                } catch (OponentsBufferFullException ex) {
                    finished = false;
                    Thread.sleep(50);
                }
            } while (!finished);
        }
    }
    
    private void processSrscPacket(PacketType packetType, short[] payload) {
        Device device;
        Long identifier = Long.valueOf(packetType.getIdentifier());
        
        if (nonTwiSensors.containsKey(identifier)) {
            device = nonTwiSensors.get(identifier);
        } else if (twiSensors.containsKey(identifier)) {
            device = twiSensors.get(identifier);
        } else {
            Logger.warn("Packet type '{}' not known!", packetType.getIdentifier());
            return;
        }

        switch (device.getDeviceType()) {
            case LIDAR -> {
                payloadProcessor.processLidarPayload(payload, device.getMibTopic("topic"));
            }
            case SSA -> {
                payloadProcessor.processSsaPayload(payload, device.getMibTopic("front"), device.getMibTopic("bottom"));
            }
            case TACTILE -> {
                payloadProcessor.processTactilePayload(device.getMibTopic("topic"));
            }
            case LTS -> {
                payloadProcessor.processLtsPayload(payload, device.getMibTopic("topic"));
            }
            case GPS -> {
                payloadProcessor.processGpsPayload(payload, device.getMibTopic("topic"));
            }
            case HALL -> {
                payloadProcessor.processHallPayload(
                        payload,
                        device.getMibTopic("fl"),
                        device.getMibTopic("fr"),
                        device.getMibTopic("bl"),
                        device.getMibTopic("br")
                );
            }
            default -> {
                Logger.warn("Device of type '{}' can't send messages!", device.getDeviceType());
            }
        }
    }*/

    @Override
    protected void setup() throws InterruptedException {
        /*srsc.registerOnPacketArrivedCallback((packetType, payload) -> {
            processSrscPacket(packetType, payload);
        });
        
        srsc.begin(0, 1000);
        Thread.sleep(5000);
        registerSrscPacketTypes();
        
        JSONObject scuConnectedPayload = new JSONObject(Map.of(
                "name", "scuConnected",
                "value", true)
        );
        
        broker.publish(topics.getString("state.operationSet"), scuConnectedPayload, Priority.NORMAL);
        broker.publish(scuTopics.getString("connected"), Priority.NORMAL);*/
    }

    @Override
    protected void loop() throws InterruptedException {
        /*Message message = messageQueue.take();
        
        if (motors.containsKey(message.topic())) {
            Device motor = motors.get(message.topic());
            boolean finished = true;
            
            if (message.payload().has("value")) {
                do {
                    try {
                        short[] payload = {(short) message.payload().getInt("value")};
                        
                        srsc.writePacket(motor.getSrscPacketType(), payload);
                        finished = true;
                    } catch (PacketTypeNotRegisteredException | WrongPayloadLengthException | NotConnectedException | JSONException ex) {
                        Logger.error("{}: error: '{}'", getModuleName(), ex.getMessage());
                    } catch (OponentsBufferFullException ex) {
                        finished = false;
                        Thread.sleep(50);
                    }
                } while (!finished);
            }
        }*/
    }

    public ScuModule(/*Srsc srsc*/) {
        super("ScuModule");
        /*this.srsc = srsc;
        conversionService = new DefaultConversionServiceFactory().getService();
        payloadProcessor = new SrscPayloadProcessor(broker);*/
    }
}