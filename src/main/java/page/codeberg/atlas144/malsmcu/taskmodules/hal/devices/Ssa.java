// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 *
 * @author atlas144
 */
public class Ssa extends TwiDevice {
    private static final int DATA_SIZE = 2;
    private static final int MEASUREMENT_PERIOD = 60;
    
    private final String frontTopic;
    private final String bottomTopic;
    
    public Ssa(Context context, MalsIntermoduleBroker broker, int address, String frontTopic, String bottomTopic) {
        super(context, broker, address, DATA_SIZE, MEASUREMENT_PERIOD);
        
        this.frontTopic = frontTopic;
        this.bottomTopic = bottomTopic;
    }
    
    @Override
    protected void processData(byte[] data) {
        short frontDistance = data[0];
        short bottomDistance = data[1];
        
        broker.publish(frontTopic, frontDistance, Priority.NORMAL);
        broker.publish(bottomTopic, bottomDistance, Priority.NORMAL);
    }
}
