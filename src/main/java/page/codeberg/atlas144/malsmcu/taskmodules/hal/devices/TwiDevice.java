// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal.devices;

import com.pi4j.context.Context;
import com.pi4j.io.i2c.I2C;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;

/**
 *
 * @author atlas144
 */
public abstract class TwiDevice extends Thread {
    private static final String PROVIDER = "pigpio-i2c";
    private static final int BUS = 1;
    private static final int REGISTER = 0;
    
    protected final MalsIntermoduleBroker broker;
    private final I2C device;
    private final int dataSize;
    private final long measurementPeriod;

    public TwiDevice(Context context, MalsIntermoduleBroker broker, int address, int dataSize, long measurementPeriod) {
        this.broker = broker;
        this.dataSize = dataSize;
        this.measurementPeriod = measurementPeriod;
        device = context.create(I2C.newConfigBuilder(context)
            .bus(BUS)
            .device(address)
            .provider(PROVIDER)
            .build()
        );
    }
    
    protected abstract void processData(byte[] data); 

    @Override
    public void run() {
        while (true) {   
            try {
                Thread.sleep(measurementPeriod);
                
                if (device.isOpen()) {
                    byte[] buffer = new byte[dataSize];

                    device.readRegister(REGISTER, buffer);

                    processData(buffer);
                }                
            } catch (InterruptedException ex) {}
        }
    }
}
