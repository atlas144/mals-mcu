// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.scu;

/**
 *
 * @author atlas144
 */
public enum DeviceType {
    MOTOR,
    LIDAR,
    SSA,
    TACTILE,
    LTS,
    GPS,
    HALL
}
