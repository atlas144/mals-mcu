// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

/**
 *
 * @author atlas144
 */
public enum Storage {
    PERSISTENT("persistent"),
    OPERATION("operation");
    
    public final String name;

    private Storage(String name) {
        this.name = name;
    }
}
