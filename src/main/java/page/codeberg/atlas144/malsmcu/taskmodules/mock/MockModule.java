// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.mock;

import com.moandjiezana.toml.Toml;
import java.util.Map;
import org.json.JSONObject;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;
import page.codeberg.atlas144.malsmcu.config.MibTopics;

/**
 *
 * @author atlas144
 */
public class MockModule extends TaskModule {
    private final Toml topics = MibTopics.getInsance().getToml();
    private final Toml twiTopics = topics.getTable("scu.twiSensors");
    private final Toml nonTwiTopics = topics.getTable("scu.nonTwiSensors");
        
    private int lidarAngle = 0;
    private boolean lidarAngleInc = true;
    private int tactileCounter = 0;
    private boolean tactileCounterInc = true;

    public MockModule() {
        super("MockModule");
    }

    @Override
    protected void setup() throws InterruptedException {}
    
    private double getRandomValue(double min, double max) {
        return Math.random() * (max - min) + min;
    }

    @Override
    protected void loop() throws InterruptedException {
        if (lidarAngleInc) {
            ++lidarAngle;
        } else {
            --lidarAngle;
        }
            
        if (lidarAngle >= 60 || lidarAngle <= 0) {
            lidarAngleInc = !lidarAngleInc;
        }
        
        broker.publish(twiTopics.getString("lidar"), new JSONObject(Map.of("distance", getRandomValue(30, 120), "angle", lidarAngle)), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaFlf"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaFlb"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaFrf"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaFrb"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaBlf"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaBlb"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaBrf"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaBrb"), getRandomValue(20, 100), Priority.NORMAL);
        broker.publish(twiTopics.getString("ssaBrb"), getRandomValue(20, 100), Priority.NORMAL);
        
        if (tactileCounter == 10) {
            broker.publish(nonTwiTopics.getString("tactileBack"), Priority.NORMAL);
            Logger.info("back contact published");
        } else if (tactileCounter == 0) {
            broker.publish(nonTwiTopics.getString("tactileFront"), Priority.NORMAL);
            Logger.info("front contact published");
        }
        
        if (tactileCounterInc) {
            ++tactileCounter;
        } else {
            --tactileCounter;
        }
        
        if (tactileCounter >= 10 || tactileCounter <= 0) {
            tactileCounterInc = !tactileCounterInc;
        }
        
        sleep(500);
    }
}
