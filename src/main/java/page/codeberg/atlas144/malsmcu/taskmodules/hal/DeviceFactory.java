// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.taskmodules.hal;

import com.moandjiezana.toml.Toml;
import com.pi4j.context.Context;
import java.util.Map;
import java.util.stream.Collectors;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Hall;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Lidar;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Motor;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Ssa;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.devices.Tactile;

/**
 *
 * @author atlas144
 */
public class DeviceFactory {
    private final Context context;
    
    public DeviceFactory(Context context) {
        this.context = context;
    }
    
    private Motor buildMotor(String id, Map<String, Object> motorToml) {
        return new Motor(
                context, 
                id, 
                (int) ((long) motorToml.get("fwPin")), 
                (int) ((long) motorToml.get("bwPin"))
        );
    }
    
    public Map<String, Motor> buildMotors(Toml motorsToml) {
        return motorsToml
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> buildMotor(entry.getKey(), (Map) entry.getValue())
                ));
    }
    
    private Hall buildHall(MalsIntermoduleBroker broker, Toml nonTwiSensorTopics, Map<String, Object> hallToml) {
        return new Hall(
                context, 
                broker,
                (int) ((long) hallToml.get("pin")), 
                nonTwiSensorTopics.getString((String) hallToml.get("mibTopicShortAlias"))
        );
    }
    
    public Map<String, Hall> buildHalls(MalsIntermoduleBroker broker, Toml nonTwiSensorTopics, Toml hallsToml) {
        return hallsToml
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> buildHall(broker, nonTwiSensorTopics, (Map) entry.getValue())
                ));
    }
    
    private Tactile buildTactile(String id, MalsIntermoduleBroker broker, Toml nonTwiSensorTopics, Map<String, Object> tactileToml) {
        return new Tactile(
                context,
                id,
                broker,
                (int) ((long) tactileToml.get("pin")), 
                nonTwiSensorTopics.getString((String) tactileToml.get("mibTopicShortAlias"))
        );
    }
    
    public Map<String, Tactile> buildTactiles(MalsIntermoduleBroker broker, Toml nonTwiSensorTopics, Toml tactilesToml) {
        return tactilesToml
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> buildTactile(entry.getKey(), broker, nonTwiSensorTopics, (Map) entry.getValue())
                ));
    }
    
    private Lidar buildLidar(MalsIntermoduleBroker broker, Toml twiSensorTopics, Map<String, Object> lidarToml) {
        return new Lidar(
                context,
                broker,
                (int) ((long) lidarToml.get("twiAddress")), 
                twiSensorTopics.getString((String) lidarToml.get("mibTopicShortAlias"))
        );
    }
    
    public Map<String, Lidar> buildLidars(MalsIntermoduleBroker broker, Toml twiSensorsTopics, Toml lidarsToml) {
        return lidarsToml
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> buildLidar(broker, twiSensorsTopics, (Map) entry.getValue())
                ));
    }
    
    private Ssa buildSsa(MalsIntermoduleBroker broker, Toml twiSensorTopics, Map<String, Object> ssaToml) {
        return new Ssa(
                context,
                broker,
                (int) ((long) ssaToml.get("twiAddress")),
                twiSensorTopics.getString((String) ssaToml.get("frontMibTopicShortAlias")),
                twiSensorTopics.getString((String) ssaToml.get("bottomMibTopicShortAlias"))
        );
    }
    
    public Map<String, Ssa> buildSsa(MalsIntermoduleBroker broker, Toml twiSensorsTopics, Toml ssaToml) {
        return ssaToml
                .toMap()
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> buildSsa(broker, twiSensorsTopics, (Map) entry.getValue())
                ));
    }
}
