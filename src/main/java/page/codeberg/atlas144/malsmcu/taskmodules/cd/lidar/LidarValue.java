// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.cd.lidar;

/**
 *
 * @author atlas144
 */
public enum LidarValue {
    
    DISTANCE,
    ANGLE
    
}
