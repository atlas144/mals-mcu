// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.cli;

import java.text.MessageFormat;
import org.tinylog.Logger;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBroker;
import page.codeberg.atlas144.malsintermodulebroker.MalsIntermoduleBrokerInjector;
import page.codeberg.atlas144.malsmcu.taskmodules.cd.CdModule;
import page.codeberg.atlas144.malsmcu.taskmodules.hal.HalModule;
import page.codeberg.atlas144.malsmcu.taskmodules.ltcontrol.LtControlModule;
import page.codeberg.atlas144.malsmcu.taskmodules.manualcontrol.ManualControlModule;
import page.codeberg.atlas144.malsmcu.taskmodules.mock.MockModule;
import page.codeberg.atlas144.malsmcu.taskmodules.state.StateModule;
import page.codeberg.atlas144.malsmcu.taskmodules.websocket.WebSocketModule;

/**
 *
 * @author atlas144
 */
public class Cli {
    private static final String HELP_ARG = "help";
    private static final String PROD_ARG = "prod";
    private static final String MOCK_ARG = "mock";
    
    private final String[] args;

    public Cli(String[] args) {
        this.args = args;
    }
    
    private void executeHelp() {
        System.out.println(MessageFormat.format("""
            java -jar mals-mcu.jar <run_mode>
            
            Run modes:
            {0} - prints this message
            {1} - runs production build
            {2} - runs mock build""", HELP_ARG, PROD_ARG, MOCK_ARG));
    }
    
    private void executeProd() {
        Logger.info("starting production build...");
        MalsIntermoduleBroker broker = MalsIntermoduleBrokerInjector.getMalsIntermoduleBroker();
        
        WebSocketModule webSocketModule = new WebSocketModule();
        HalModule halModule = new HalModule();
        CdModule cdModule = new CdModule();
        ManualControlModule manualControlModule = new ManualControlModule();
        LtControlModule ltControlModule = new LtControlModule();
        StateModule stateModule = new StateModule();

        broker.registerModule(halModule);
        broker.registerModule(webSocketModule);
        broker.registerModule(cdModule);
        broker.registerModule(manualControlModule);
        broker.registerModule(ltControlModule);
        broker.registerModule(stateModule);
        
        broker.start();
    }
    
    private void executeMock() {
        Logger.info("starting mock build...");
        MalsIntermoduleBroker broker = MalsIntermoduleBrokerInjector.getMalsIntermoduleBroker();
        
        WebSocketModule webSocketModule = new WebSocketModule();
        MockModule mockModule = new MockModule();
        
        broker.registerModule(webSocketModule);
        broker.registerModule(mockModule);
        
        broker.start();
    }
    
    public void run() {
        if (args.length > 0) {
            switch (args[0]) {
                case HELP_ARG -> executeHelp();
                case PROD_ARG -> executeProd();
                case MOCK_ARG -> executeMock();
                default -> Logger.error("unknown run mode '{}'", args[0]);
            }
        } else {
            executeHelp();
        }
    }
}
