// SPDX-License-Identifier: GPL-3.0-only

package page.codeberg.atlas144.malsmcu;

import page.codeberg.atlas144.malsmcu.cli.Cli;

/**
 *
 * @author atlas144
 */
public class MalsMcu {
    
    public static void main(String[] args) {
        Cli cli = new Cli(args);
        
        cli.run();
    }
    
}
