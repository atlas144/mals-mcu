// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.model;

import com.moandjiezana.toml.Toml;

/**
 *
 * @author atlas144
 */
public class VirtualHall {
    private final String mibTopic;
    private final ComponentPosition position;
    
    public VirtualHall(Toml toml) {
        mibTopic = toml.getString("topic");
        position = ComponentPosition.valueOf(toml.getString("position").toUpperCase());
    }

    public String getMibTopic() {
        return mibTopic;
    }

    public ComponentPosition getPosition() {
        return position;
    }
}
