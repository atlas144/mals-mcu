// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.model;

/**
 *
 * @author atlas144
 */
public enum HorizontalPosition {
    LEFT,
    RIGHT 
}
