// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.model;

/**
 *
 * @author atlas144
 */
public class MotorConfiguration {
    private MotorDirection direction;
    private int speed;

    public MotorConfiguration(MotorDirection direction, int speed) {
        this.direction = direction;
        this.speed = speed;
    }

    public MotorDirection getDirection() {
        return direction;
    }

    public void setDirection(MotorDirection direction) {
        this.direction = direction;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
