// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.malsmcu.model;

/**
 *
 * @author atlas144
 */
public enum ComponentPosition {
    FL(VerticalPosition.FRONT, HorizontalPosition.LEFT),
    FR(VerticalPosition.FRONT, HorizontalPosition.RIGHT),
    BL(VerticalPosition.BACK, HorizontalPosition.LEFT),
    BR(VerticalPosition.BACK, HorizontalPosition.RIGHT);
    
    private final VerticalPosition verticalPosition;
    private final HorizontalPosition horizontalPosition;
    
    public HorizontalPosition getHorizontalPosition() {
        return horizontalPosition;
    }
    
    public VerticalPosition getVerticalPosition() {
        return verticalPosition;
    }
    
    private ComponentPosition(VerticalPosition verticalPosition, HorizontalPosition horizontalPosition) {
        this.verticalPosition = verticalPosition;
        this.horizontalPosition = horizontalPosition;
    }
}
