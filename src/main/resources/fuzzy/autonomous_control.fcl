FUNCTION_BLOCK autonomousControl

VAR_INPUT
	distance : REAL;
	angle : REAL;
END_VAR

VAR_OUTPUT
	leftSpeed : REAL;
	rightSpeed : REAL;
END_VAR

FUZZIFY distance
	TERM none := (0, 1) (100, 0); 
	TERM small := (0, 0) (100, 1) (500, 1) (1000, 0);
	TERM large := (500, 0) (1000, 1);
END_FUZZIFY

FUZZIFY angle
	TERM back := (-180, 1) (-90, 0) (90, 0) (180, 1);
	TERM left := (-180, 0) (-90, 1) (0, 0);
	TERM front := (-90, 0) (0, 1) (90, 0);
	TERM right := (0, 0) (90, 1) (180, 0);
END_FUZZIFY

FUZZIFY obstacle
	TERM near := (0, 1) (100, 1) (200, 0); 
	TERM far := (1, 0) (200, 1);
END_FUZZIFY

DEFUZZIFY leftSpeed
	TERM backFast := (-255, 1) (-170, 1) (-85, 0);
	TERM backSlow := (-170, 0) (-85, 1) (0, 0);
	TERM stop := (-90, 0) (0, 1) (90, 0);
	TERM forSlow := (0, 0) (85, 1) (170, 0);
	TERM forFast := (85, 0) (170, 1) (255, 1);
	METHOD : COG;
	DEFAULT := 0;
END_DEFUZZIFY

DEFUZZIFY rightSpeed
	TERM backFast := (-255, 1) (-170, 1) (-85, 0);
	TERM backSlow := (-170, 0) (-85, 1) (0, 0);
	TERM stop := (-90, 0) (0, 1) (90, 0);
	TERM forSlow := (0, 0) (85, 1) (170, 0);
	TERM forFast := (85, 0) (170, 1) (255, 1);
	METHOD : COG;
	DEFAULT := 0;
END_DEFUZZIFY

RULEBLOCK No1
	AND : MIN;
	ACT : MIN;
	ACCU : MAX;

	RULE 1 : IF distance IS none AND angle IS back AND obstacle IS near THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 2 : IF distance IS none AND angle IS back AND obstacle IS far THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 3 : IF distance IS none AND angle IS left AND obstacle IS near THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 4 : IF distance IS none AND angle IS left AND obstacle IS far THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 5 : IF distance IS none AND angle IS front AND obstacle IS near THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 6 : IF distance IS none AND angle IS front AND obstacle IS far THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 7 : IF distance IS none AND angle IS right AND obstacle IS near THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 8 : IF distance IS none AND angle IS right AND obstacle IS far THEN leftSpeed IS stop, rightSpeed IS stop;
	RULE 9 : IF distance IS small AND angle IS back AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 10 : IF distance IS small AND angle IS back AND obstacle IS far THEN leftSpeed IS forFast, rightSpeed IS backFast;
	RULE 11 : IF distance IS small AND angle IS left AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 12 : IF distance IS small AND angle IS left AND obstacle IS far THEN leftSpeed IS forFast, rightSpeed IS backFast;
	RULE 13 : IF distance IS small AND angle IS front AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 14 : IF distance IS small AND angle IS front AND obstacle IS far THEN leftSpeed IS forSlow, rightSpeed IS forSlow;
	RULE 15 : IF distance IS small AND angle IS right AND obstacle IS near THEN leftSpeed IS backSlow, rightSpeed IS forSlow;
	RULE 16 : IF distance IS small AND angle IS right AND obstacle IS far THEN leftSpeed IS backSlow, rightSpeed IS forSlow;
	RULE 17 : IF distance IS large AND angle IS back AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 18 : IF distance IS large AND angle IS back AND obstacle IS far THEN leftSpeed IS forFast, rightSpeed IS backFast;
	RULE 19 : IF distance IS large AND angle IS left AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 20 : IF distance IS large AND angle IS left AND obstacle IS far THEN leftSpeed IS forFast, rightSpeed IS forFast;
	RULE 21 : IF distance IS large AND angle IS front AND obstacle IS near THEN leftSpeed IS forSlow, rightSpeed IS backSlow;
	RULE 22 : IF distance IS large AND angle IS front AND obstacle IS far THEN leftSpeed IS forFast, rightSpeed IS forFast;
	RULE 23 : IF distance IS large AND angle IS right AND obstacle IS near THEN leftSpeed IS backSlow, rightSpeed IS forSlow;
	RULE 24 : IF distance IS large AND angle IS right AND obstacle IS far THEN leftSpeed IS backFast, rightSpeed IS forFast;
END_RULEBLOCK

END_FUNCTION_BLOCK
