// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import java.time.LocalDateTime;
import java.util.Map;

/**
 *
 * @author atlas144
 */
public class MockStateDal implements StateDal {
    private final Map<String, Field> persistentFields;
    private final Map<String, Field> operationFields;
    
    public MockStateDal(Map persistentFields, Map operationFields) {
        this.persistentFields = persistentFields;
        this.operationFields = operationFields;
    }
    
    @Override
    public Field getField(Storage storage, String name) throws Exception {
        switch (storage) {
            case PERSISTENT -> {
                if (persistentFields.containsKey(name)) {
                    return persistentFields.get(name);
                } else {
                    throw new Exception();
                }
            }
            case OPERATION -> {
                if (operationFields.containsKey(name)) {
                    return operationFields.get(name);
                } else {
                    throw new Exception();
                }
            }
            default -> throw new Exception();
        }
    }

    @Override
    public boolean checkField(Storage storage, String name) throws Exception {
        switch (storage) {
            case PERSISTENT -> {return persistentFields.containsKey(name);}
            case OPERATION -> {return operationFields.containsKey(name);}
            default -> throw new Exception();
        }
    }

    @Override
    public void updateField(Storage storage, String name, String value) throws Exception {
        switch (storage) {
            case PERSISTENT -> {
                persistentFields.put(name, new Field(name, value, LocalDateTime.now()));
            }
            case OPERATION -> {
                operationFields.put(name, new Field(name, value, LocalDateTime.now()));
            }
            default -> throw new Exception();
        }
    }

    @Override
    public void createField(Storage storage, String name, String value) throws Exception {
        updateField(storage, name, value);
    }

    @Override
    public void clearOperationStorage() throws Exception {
        operationFields.clear();
    }
}
