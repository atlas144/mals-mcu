// SPDX-License-Identifier: MIT

package page.codeberg.atlas144.malsmcu.taskmodules.state;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author atlas144
 */
public class StorageServiceTest {
    private static final String NEW_NAME = "testName";
    private static final String NEW_VALUE = "testValue";
    private static final String PERSISTENT_NAME = "persistentName";
    private static final String PERSISTENT_VALUE = "persistentValue";
    private static final String OPERATION_NAME = "operationName";
    private static final String OPERATION_VALUE = "operationValue";
        
    private Map<String, Field> persistentFields;
    private Map<String, Field> operationFields;
    private StorageService storageService;
    
    @BeforeEach
    void setUp() {
        persistentFields = new HashMap<>();
        persistentFields.put(PERSISTENT_NAME, new Field(PERSISTENT_NAME, PERSISTENT_VALUE, LocalDateTime.now()));
        operationFields = new HashMap<>();
        operationFields.put(OPERATION_NAME, new Field(OPERATION_NAME, OPERATION_VALUE, LocalDateTime.now()));
        
        storageService = new StorageService(new MockStateDal(persistentFields, operationFields));
    }
    
    @Test
    public void testGetPersistentFieldReturnsValidValueFromStorage() throws Exception {
        assertEquals(PERSISTENT_VALUE, storageService.getPersistentField(PERSISTENT_NAME).value());
    }
    
    @Test
    public void testGetPersistentFieldThrowsWhenFieldIsMissing() throws Exception {
        String nameValue = "nonexisting";
        
        assertThrows(Exception.class, () -> {storageService.getPersistentField(nameValue).value();});
    }
    
    @Test
    public void testGetOperationFieldReturnsValidValueFromStorage() throws Exception {
        assertEquals(OPERATION_VALUE, storageService.getOperationField(OPERATION_NAME).value());
    }
    
    @Test
    public void testGetOperationFieldThrowsWhenFieldIsMissing() throws Exception {
        String nameValue = "nonexisting";
        
        assertThrows(Exception.class, () -> {storageService.getOperationField(nameValue).value();});
    }

    @Test
    public void testCheckPersistentFieldReturnstTrueForPresentField() throws Exception {
        assertTrue(storageService.checkPersistentField(PERSISTENT_NAME));
    }

    @Test
    public void testCheckPersistentFieldReturnstFalseForMissingField() throws Exception {
        String nameValue = "nonexisting";
        
        assertTrue(!storageService.checkPersistentField(nameValue));
    }

    @Test
    public void testCheckOperationFieldReturnstTrueForPresentField() throws Exception {
        assertTrue(storageService.checkOperationField(OPERATION_NAME));
    }

    @Test
    public void testCheckOperationFieldReturnstFalseForMissingField() throws Exception {
        String nameValue = "nonexisting";
        
        assertTrue(!storageService.checkOperationField(nameValue));
    }
    
    @Test
    public void testSetPersistentFieldAddsNewFieldCorrectly() throws Exception {
        storageService.setPersistentField(NEW_NAME, NEW_VALUE);
        
        Field actualField = persistentFields.get(NEW_NAME);
        
        assertEquals(NEW_NAME, actualField.name());
        assertEquals(NEW_VALUE, actualField.value());
    }

    @Test
    public void testSetOperationFieldAddsNewFieldCorrectly() throws Exception {
        storageService.setOperationField(NEW_NAME, NEW_VALUE);
        
        Field actualField = operationFields.get(NEW_NAME);
        
        assertEquals(NEW_NAME, actualField.name());
        assertEquals(NEW_VALUE, actualField.value());
    }
    
    @Test
    public void testSetPersistentFieldUpdatesExistingFieldCorrectly() throws Exception {
        storageService.setPersistentField(PERSISTENT_NAME, NEW_VALUE);
        
        Field actualField = persistentFields.get(PERSISTENT_NAME);
        
        assertEquals(PERSISTENT_NAME, actualField.name());
        assertEquals(NEW_VALUE, actualField.value());
    }

    @Test
    public void testSetOperationFieldUpdatesExistingFieldCorrectly() throws Exception {
        storageService.setOperationField(OPERATION_NAME, NEW_VALUE);
        
        Field actualField = operationFields.get(OPERATION_NAME);
        
        assertEquals(OPERATION_NAME, actualField.name());
        assertEquals(NEW_VALUE, actualField.value());
    }

    @Test
    public void testClearOperationStorageWorksCorrectly() throws Exception {
        storageService.clearOperationStorage();
        
        assertTrue(operationFields.isEmpty());
    }
}
